//
//  videoLoader.h
//  EyeTrack_Updated
//
// This file controls the methods and functions that
// load all the video and camera inputs inside of videoLoader.cpp

#ifndef EyeTrack_Updated_videoLoader_h
#define EyeTrack_Updated_videoLoader_h

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxOpenCv.h"

enum {
    IN_LIVE_VID, IN_VID
};

class videoLoader {
    public:
    
        // functions inside of videoLoader.cpp
        void setup();
        void update();
    
        // change the mode of the video to one of the enum values
        int mode;

        ofVideoGrabber grabber;         // the component responsible for grabbing video
        ofVideoPlayer player;           // the component responsible for playing preloaded videos
        int frame_width, frame_height;  // frame width and height
    
        ofxCvColorImage colorImage;     // image that holds the original cam image
        
		// image that holds a grayscaled version of the camera image, I use this
        ofxCvGrayscaleImage grayImage;
    
        bool newFrameGrabbed;           // tell if we grabbed a new frame on update();
    
};

#endif
