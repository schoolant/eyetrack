//
//  videoLoader.cpp
//  EyeTrack_Updated
//
// Responsible for loading the video components
// into the openGL instance window for further
// processing

#include "videoLoader.h"

// what happens when this component is loaded?
void videoLoader::setup() {
    ofxXmlSettings xml;
    xml.loadFile("../../src/settings/videoSettings.xml");
    mode = xml.getValue("video:mode", 0);
    
    // for pre-recorded video input
    if(mode == IN_VID) {
        cout << "loading a video file" << endl;
        string vidToLoad = xml.getValue("video:vid_file", "");
        player.loadMovie(vidToLoad);
        player.play();
        frame_width = player.width;
        frame_height = player.height;
    }
    
    // for live video input
    if(mode == IN_LIVE_VID) {
        // this is for real time video collection
        
        // try and get the frame width/height from the settings file, else use 640
        frame_width = xml.getValue("video:frame_width", 640);
        frame_height = xml.getValue("video:frame_height", 480);
        
        // defaults to port 0, probably need 1 on laptops with webcams
        int inputId = xml.getValue("video:inputId", 0);
        
        // if the input id is not 0, change it in the camera setup
        if(inputId != 0) {
            grabber.setDeviceID(inputId);
        }
        
        // init video capture with no textures, this is a new addition!
        grabber.initGrabber(frame_width, frame_height, false);
        
        // width and height take effect on capture, smaller area = faster
        frame_width = grabber.width;
        frame_height = grabber.height;
    }
    
    if(frame_width != 0 || frame_height != 0) {
        // turn off textures for faster rendering onscreen, not needed-faster
        colorImage.setUseTexture(false); 
        grayImage.setUseTexture(false);
        // create images based on our camera frame sizes
        colorImage.allocate(frame_width, frame_height);
        grayImage.allocate(frame_width, frame_height);
    }
    
    newFrameGrabbed = false;
}

// what happens when the component is updated?
void videoLoader::update() {
    if(mode == IN_LIVE_VID) {
        // start grabbing the frames!
        grabber.grabFrame();
        
        // check if the frame is new using the grabber
        newFrameGrabbed = grabber.isFrameNew();
    } else {
        player.idleMovie();
        newFrameGrabbed = player.isFrameNew();
    }
    
    // when a new frame is grabbed, convert to grayscale
    if(newFrameGrabbed) {
        if(mode == IN_LIVE_VID) {
            colorImage.setFromPixels(grabber.getPixels(), frame_width, frame_height);
        } else {
            colorImage.setFromPixels(player.getPixels(), frame_width, frame_height);
        }
        grayImage = colorImage;
    }
}