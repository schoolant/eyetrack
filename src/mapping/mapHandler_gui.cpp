/**
* File manages and controls all the gui elements
* of the mapping system.
* Allows for editing variables without
* things getting too complicated and adding
* unecessary header files.
*/

#include "mapHandler.h"

void mapHandler::loadGui() {
   gui.setup("mapping", ofGetWidth() - 280, 10, 270, 500);
   gui.addPanel("mapping setup", 1, false);
   gui.addPanel("autoset mapping", 1, false);
   gui.addPanel("mapping display settings", 1, false);
   
   // Mappping Settings
   gui.setWhichPanel("mapping setup");
   gui.addToggle(" Remove non-average data", "RMV_NON_AVG_DATA", false);
   
   gui.addSlider("Horizontal Calculations", "C_HOR", nDivW, 2, 20, true);
   gui.addSlider("Vertical Calculations", "C_VERT", nDivH, 2, 20, true);
   
   gui.addSlider("Blob smoothing", "BLOB_SMOOTHING", 0.990, 0.8, 2.0f, false);
   gui.addSlider("Fix outstanding blobs", "REMOVE_OUTSTAND", 2, 0.1, 4, false);
   
   gui.addSlider("Time for pre record", "PRE_REC_T", pre_t_dot, 0.1, 5, false);
   gui.addSlider("Time for point capture", "PNT_CAP_T", rec_t_dot, 0.1, 2.0, false);
   
   // AutoSet Mapping
   gui.setWhichPanel("autoset mapping");
   gui.addToggle(" Start auto mapping test", "START_A_MAPPING", false);
   gui.addToggle(" Hardware Simulation", "HARDWARE_SIM", false);
   
   gui.addToggle(" Save map", "SAVE_MAP", false);
   gui.addToggle(" Load map", "LOAD_MAP", false);
   gui.addToggle(" Reset map", "RESET_MAP", false);
   
   // load gui settings from...
   gui.loadSettings("../../src/settings/mapGuiSettings.xml");
   
   // mapping display settings
   gui.setWhichPanel("mapping display settings");
   //gui.addToggle(" Draw raw input", "DRAW_RAW_IN", false);
   //gui.addToggle(" Draw pupil input", "DRAW_P_INPUT", false);
   //gui.addToggle(" Draw raw map input", "DRAW_RAW_MAP_IN", false);
   //gui.addToggle(" Draw output map", "DRAW_OUT_MAP", false);
   gui.addToggle(" Draw map", "DRAW_ERROR", false);
   
   gui.addSlider("Data scaling", "DATA_SCALE", 20, 10, 100, false);
   gui.addSlider("Data offset X", "OFF_X", ofGetWidth() / 2, 0, ofGetWidth() * 2, true);
   gui.addSlider("Data offset Y", "OFF_Y", ofGetHeight() / 2, 0, ofGetHeight() * 2, true);
   
}

void mapHandler::updateGui() {
    gui.update();
    
    // W * H calculations
    nDivH = gui.getValueI("C_VERT");
    nDivW = gui.getValueI("C_HOR");
    
    // capture and pre times
    pre_t_dot = gui.getValueF("PRE_REC_T");
    rec_t_dot = gui.getValueF("PNT_CAP_T");
    
    // averages
    bool_removePointsFarAvg = gui.getValueB("RMV_NON_AVG_DATA", false);
    
    // smoothing and outstanding
    smoothing = gui.getValueF("BLOB_SMOOTHING");
    ptDraw.removeOutliersf = gui.getValueF("REMOVE_OUTSTAND");
    
    // mapping and display settings
    // along with the offsets and outputs
    //ptDraw.bool_drawRawMapInput = gui.getValueB("DRAW_RAW_MAP_IN");
    //bool_drawMapOut             = gui.getValueB("DRAW_OUT_MAP");
    //bool_drawRawIn              = gui.getValueB("DRAW_RAW_IN");
    //bool_drawEyeIn              = gui.getValueB("DRAW_P_INPUT");
    ptDraw.bool_drawError       = gui.getValueB("DRAW_ERROR");
    
    ptDraw.rawDataScale         = gui.getValueF("DATA_SCALE");
    ptDraw.rawDataOffset.x      = gui.getValueF("OFF_X");
    ptDraw.rawDataOffset.y      = gui.getValueF("OFF_Y");
    
    // autoset mapping
    if(gui.getValueB("START_A_MAPPING")) {
        bool_preAuto = true;
        gui.setValueB("START_A_MAPPING", false);
    }
    
    // save map
    if(gui.getValueB("SAVE_MAP")) {
        if(ptDraw.bool_hasFit)
            ptDraw.saveMapping();
        gui.setValueB("SAVE_MAP", false);
    }
    
    // load map
    if(gui.getValueB("LOAD_MAP")) {
        ptDraw.loadMapping();
        gui.setValueB("LOAD_MAP", false);
    }
    
    // call clear() and reset map
    if(gui.getValueB("RESET_MAP")) {
        clear();
        gui.setValueB("RESET_MAP", false);
    }
}