// throws the data in a file for
// easy references and extraction

#include "mappingFileManager.h"

void mappingFileManager::saveCal(ofxLeastSquares &ls, int refPointsN, vector<ofPoint> &refPoints, vector<ofPoint> &lsPoints, vector<ofPoint> &eyePoints, vector<ofPoint> &drawPoints) {
    ofxXmlSettings xml;
    map = ls.getMap();
    
    for(int i = 0; i < map.size(); i++) {
        xml.addTag("term");
        xml.pushTag("term", i);
        
        vector<float> &cur = map[i]; // curent coords
        
        xml.addValue("x", cur[0]);
        xml.addValue("y", cur[1]);
        xml.popTag();
    }
    
    xml.addValue("NumRefPoints", refPointsN); // number of referenced points
    
    for(int i = 0; i < refPointsN; i++) {
        xml.addTag("refPoint"); // referenced points
        xml.pushTag("refPoint", i);
        xml.addValue("x", refPoints[i].x);
        xml.addValue("y", refPoints[i].y);
        xml.popTag();
        
        xml.addTag("avgInput");
        xml.pushTag("avgInput", i);
        xml.addValue("x", lsPoints[i].x);
        xml.addValue("y", lsPoints[i].y);
        xml.popTag();
    }
    
    xml.addValue("rawEyePoints", (int)eyePoints.size());
    
    for(int i = 0; i < eyePoints.size(); i++) {
        xml.addTag("eyePt"); // where the eye is from the camera view
        xml.pushTag("eyePt", i);
        xml.addValue("x", eyePoints[i].x);
        xml.addValue("y", eyePoints[i].y);
        xml.popTag();
    }
    
    xml.addValue("refDrawPoints", (int)drawPoints.size());
    
    for(int i = 0; i < eyePoints.size(); i++) {
        xml.addTag("drawPts"); // where the eye should be drawn onscreen
        xml.pushTag("drawPts", i);
        xml.addValue("x", drawPoints[i].x);
        xml.addValue("y", drawPoints[i].y);
        xml.popTag();
    }
    
    xml.saveFile("../../src/settings/mapData.xml"); // raw calibration data
}

void mappingFileManager::loadCal(ofxLeastSquares &ls, vector<ofPoint> &refPoints, vector<ofPoint> &lsPoints, vector<ofPoint> &eyePoints, vector<ofPoint> &drawPoints) {
    eyePoints.clear();
    drawPoints.clear();
    
    ofxXmlSettings xml;
    xml.loadFile("../../src/settings/mapData.xml");
    vector<vector<float> > map;
    
    for(int i = 0; i < 6; i++) {
        xml.pushTag("term", i);
        vector<float> cur;
        cur.push_back(xml.getValue("x", 0.0f));
        cur.push_back(xml.getValue("y", 0.0f));
        map.push_back(cur);
        xml.popTag();
    }
    
    for(int i = 0; i < xml.getValue("NumRefPoints", 0); i++) {
        xml.pushTag("refPoint", i);
        refPoints[i].x = xml.getValue("x", 0.0f);
        refPoints[i].y = xml.getValue("y", 0.0f);
        xml.popTag();
        
        xml.pushTag("avgInput", i);
        lsPoints[i].x = xml.getValue("x", 0.0f);
        lsPoints[i].y = xml.getValue("y", 0.0f);
        xml.popTag();
    }
    
    for(int i = 0; i < xml.getValue("rawEyePoints", 0); i++) {
        xml.pushTag("eyePt", i);
        eyePoints.push_back(ofPoint(xml.getValue("x", 0.0f), xml.getValue("y", 0.0f)));
        xml.popTag();
    }
    
    for(int i = 0; i < xml.getValue("refDrawPoints", 0); i++) {
        xml.pushTag("drawPts", i);
        drawPoints.push_back(ofPoint(xml.getValue("x", 0.0f), xml.getValue("y", 0.0f)));
        xml.popTag();
    }
    
    ls.setMap(map);
}
