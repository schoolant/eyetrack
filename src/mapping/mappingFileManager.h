// Mapping file manager, so I dont have to waste
// time remapping every time I want to show someone.

#ifndef MAPPINGFILEMANAGER_H_INCLUDED
#define MAPPINGFILEMANAGER_H_INCLUDED

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxLeastSquares.h"

class mappingFileManager {
    public:
    
    void saveCal(ofxLeastSquares &ls, int refPointsN, vector<ofPoint> &refPoints, vector<ofPoint> &lsPoints, vector<ofPoint> &eyePoints, vector<ofPoint> &drawPoints);
    void loadCal(ofxLeastSquares &ls, vector<ofPoint> &refPoints, vector<ofPoint> &lsPoints, vector<ofPoint> &eyePoints, vector<ofPoint> &drawPoints);
    
    vector<vector<float> > map;
};

#endif
