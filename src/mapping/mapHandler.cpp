/**
* Manages the mapping components including all of the initilizations
* and mapTracking stepping functions.
*
* Most of these functions can be edited inside of the mapping GUI
*/

#include "mapHandler.h"

mapHandler::mapHandler() {
    
}

void mapHandler::setup() {
    nDivW = 3; // 3 points of map testing on the x and y axis
    nDivH = 3;
    
    nPos = 0; // positions of the dataPoints
    pos = 0;
    
    power = 0;
    
    // shut all the booleans off
    bool_auto = false;
    bool_preAuto = false;
    bool_inAutoMode = false;
    
    // set time limits for each phase of
    // map testing
    start_t = ofGetElapsedTimef();
    pre_t_dot = 2.0; // amount of time per point
    rec_t_dot = 0.5; // the time we record the eye per dot
    total_t_dot = pre_t_dot + rec_t_dot;
    
    // not in auto record mode with animation
    bool_autoRecording = false; // beacuse we aren't doing anything yet
    autoPct = 0;
    
    // set to the window dimensions to draw a correct grid
    // and map on the correct space
    mapRect.x = 0;
    mapRect.y = 0;
    mapRect.width = 1024;
    mapRect.height = 768;
    
    // load the gui
    loadGui();
    
    // setup the point fitter
    ptDraw.setup(nDivW, nDivH);
    
    // smoothings
    smoothing = 2.0f;
    menuPower = 1;
    
    bool_preAuto = false;
}

// what we do when we start the mapping
void mapHandler::start() {
    bool_auto = true;
    bool_inAutoMode = true;
    start_t = ofGetElapsedTimef();
    ptDraw.startMap();
}

// what's called when mapping is stopped
void mapHandler::stop() {
    bool_auto = false;
    bool_preAuto = false;
    bool_inAutoMode = false;
    
    // reset all positions
    start_t = ofGetElapsedTimef();
    nPos = 0; pos = 0;
}

// what's called on updates
void mapHandler::update() {
    mapRect.x = 0;
    mapRect.y = 0;
    mapRect.width = ofGetWidth();
    mapRect.height = ofGetHeight();
    
    float wp = ofGetWidth() * 0.025f;
    float hp = ofGetHeight() * 0.025f;
    
    // map padding
    mapRect.x += wp;
    mapRect.y += hp;
    mapRect.width -= wp * 2;
    mapRect.height -= hp * 2;
    
    // update the gui with events
    updateGui();
    
    // remove the outstanding points on the fitter
    // i.e: other contours
    ptDraw.removeOutliers();
    
    total_t_dot = pre_t_dot + rec_t_dot;
    
    // set the menupower
    if((bool_auto == true && bool_inAutoMode == true) || bool_preAuto) {
        menuPower = 0.94f * menuPower + 0.06f * 0.0f;
    } else {
        menuPower = 0.94f * menuPower + 0.06f * 1.0f;
    }
    
    // start the auto calibration
    // consists of the circle moving along the invisible grid
    // at certain time intervals.
    
    if(bool_auto == true && bool_inAutoMode == true) {
        int ptsNum = nDivW * nDivH;
        float total_time = total_t_dot * ptsNum;
        
        if(ofGetElapsedTimef() - start_t > total_time) {
            bool_inAutoMode = false;
            bool_autoRecording = false;
            bool_preAuto = false;
            ptDraw.calc(mapRect);
        } else {
            float diff_t = ofGetElapsedTimef() - start_t;
            pt = (int)(diff_t / total_t_dot);
            float diff_t_dot = diff_t - pt * total_t_dot;
            
            if(diff_t_dot < pre_t_dot) {
                autoPct = (diff_t_dot / pre_t_dot);
                bool_autoRecording = false;
            } else {
                autoPct = (diff_t_dot - pre_t_dot) / rec_t_dot;
                bool_autoRecording = true;
            }
            pos = pt;
        }
    }
    
    power *= 0.98f;
    
    // steps in the map
    int xx = (pos % nDivW);
    int yy = (pos / nDivW);
    
    // determine if the steps are even
    bool bool_isEven = false;
    if(yy % 2 == 0)
        bool_isEven = true;
    
    // stepping on xpos
    xp = bool_isEven ? mapRect.x + ((float) mapRect.width / (float) (nDivW - 1)) * xx : mapRect.x + (mapRect.width - ((float)mapRect.width / (float)(nDivW - 1)) * xx);
    
    // stepping on ypos
    yp = mapRect.y + mapRect.height - ((float)mapRect.height / (float)(nDivH - 1)) * yy;
    
    ptDraw.update(pt, xp, yp);
}

// how to handle drawing the maps
// actual circle mapping is done in the main file
void mapHandler::draw() {
    
    // drawing grids - DONE
    ofEnableAlphaBlending();
    ofSetColor(30, 30, 30, (int)(255 - 255 * menuPower));
    ofRect(0, 0, ofGetWidth(), ofGetHeight());
    
    // draw the gui when not mapping
    // and before the mappiing settings are applied
    // else the gui is hidden
    if(!bool_preAuto || !bool_auto)
        gui.draw();
    
    // if we have not fit point
    if(ptDraw.bool_hasFit != true) {
        ofEnableAlphaBlending();
        
        float rad = 20;
        glLineWidth(3);
        ofSetColor(255, 255, 255, 100);
        ofLine(xp, yp - rad, xp, yp + rad);
        ofLine(xp - rad, yp, xp + rad, yp);
        
        // draw the transparent "animated" circle
        ofSetColor(255, 255, 255, 255);
        if(bool_auto == true && bool_inAutoMode == true && bool_autoRecording == true) {
        } else { ofCircle(xp, yp, rad * 0.33); }
        
        ofFill(); // turn filling on
        
        // if conditions are met draw the circles
        // this condition manages the animations
        if(bool_auto == true && bool_inAutoMode == true) {
            if(bool_autoRecording) {
                ofSetColor(255, 0, 0, 200); // line color
                ofCircle(xp, yp, 28);
                
                ofSetColor(0, 0, 255); // center of the circle
                ofCircle(xp, yp, 5);
            } else {
                ofNoFill();
                ofSetColor(255, 255, 255, 150); 
                ofCircle(xp, yp, 200 - 200 * autoPct);
                ofFill();
            }
        }
        
        glLineWidth(1);
        ofFill();
        
        // drawing more "guide" circles
        if(!bool_auto)
            if(power > 0) {
                ofSetColor(255, 0, 127, (int)(60 * power));
                ofCircle(xp, yp, rad * 3,5);
                
                ofSetColor(255, 0, 127, (int)(150 * power));
                ofCircle(xp, yp, rad);
            }
        
        ofSetColor(255, 255, 255);
    }
    
    // draw the fitter's calculations
    ptDraw.draw();
}

// clear and reset everything
void mapHandler::clear() {
    pos = 0;
    ptDraw.clear();
}

// advance foward positions
void mapHandler::fowardPos() {
    pos++;
    pos %= (nDivW * nDivH);
    power = 0;
}

// next 3 functions are for gui handlers
void mapHandler::mouseDragged(int x, int y, int button) {
    gui.mouseDragged(x, y, button);
}

void mapHandler::mousePressed(int x, int y, int button) {
    gui.mousePressed(x, y, button);
}

void mapHandler::mouseReleased(int x, int y, int button) {
    gui.mouseReleased();
}

// handle keypresses
void mapHandler::keyPressed(int key) {
    if(bool_preAuto == true) {
        // enter key * will trigger.
        if(key == OF_KEY_RETURN) {
            bool_preAuto = false;
            start();
        }
    } else if (bool_preAuto == false && !bool_inAutoMode) {
        if(key == OF_KEY_RETURN) {
            bool_preAuto = true;
        }
    } else if(key == OF_KEY_RETURN) {
        clear();
        stop();
    }
}


















