//
//  mapHandler.h
//  EyeTrack_Updated
//
// Defines the functions and global variables in the cpp file
// mapHandler.cpp

#ifndef EyeTrack_Updated_mapHandler_h
#define EyeTrack_Updated_mapHandler_h

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxLeastSquares.h"
#include "fitPupil.h"
#include "ofxControlPanel.h"

class mapHandler {
public:
    mapHandler();
    
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    
    // gui stuffs
    void loadGui();
    void updateGui();
    ofxControlPanel gui;
    
    ofRectangle mapRect;
    
    void clear();
    void fowardPos();
    
    int nDivW, nDivH;
    int nPos;
    
    float xp, yp;
    int pos;
    float power;
    
    void start();
    void stop();
    
    bool bool_preAuto;
    bool bool_auto;
    bool bool_inAutoMode;
    
    // Time based variables
    float start_t;
    float pre_t_dot;
    float rec_t_dot;
    float total_t_dot;
    
    bool bool_autoRecording;
    bool bool_removePointsFarAvg;
    
    float autoPct;
    int pt;
    
    float smoothing;
    float menuPower;
    
    //drawing booleans
    bool bool_drawGrid;
    bool bool_drawEyeIn;
    bool bool_drawRawIn;
    bool bool_drawMapOut;
    
    fitPupil ptDraw;
    
};


#endif