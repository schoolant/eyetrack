//
//  fitPupil.h
//  EyeTrack_Updated
//
// Header file for fitPupil.cpp

#ifndef EyeTrack_Updated_fitPupil_h
#define EyeTrack_Updated_fitPupil_h

#include "ofMain.h"
#include "mappingFileManager.h"
#include "ofxLeastSquares.h"

/**
* Works by taking the raw data from the camera on where
* the subject's eye position is at a given frame. Takes
* the data from that and compares it to a "viewpoint" onscreen,
* then compares data with that point and tries to scale it up 3:1
* so it can draw the raw data onto the computer screen.
* complex, but not really that complex. Just a bunch of data inputs.
*/

class fitPupil {
public:
    
    // starting variables with '_' so I can use them as "temporaries"
    void setup(int _nDivW, int _nDivH);
    void update(int _currentPoint, float _xp, float _yp);
    
    // return the current mapped point (x,y)
    ofPoint getMapPoint(float x, float y);
    
    // draw the map and point
    void draw();
    void drawError();
    void drawRawMapInput(int xOff, int yOff, float scale);

    // draw all the other things (fake grid)
    void drawOtherObjects(ofRectangle & mapRect); 
    
    // clear the vars that manage the map
    void clear();
    
    // later add save and load functions? - DONE
    void saveMapping();
    void loadMapping();
    
    // begin mapping functions
    void startMap();
    vector<float> makeOut(float x, float y);
    vector<float> makeIn(float x, float y);
    
    // removing non tracked points
    // so they wont get checked during calculation
    void removeOutliers();
    void removePointsOffAverage();
    void calcErrors(ofRectangle & mapRect);
    
    // what rect input we're using for the map
    void registerMapInput(float x, float y);
    
    // scaling up the images and points
    void calcWeight(vector<ofPoint> trackedPoints, vector<ofPoint> knownPoints);
    void calc(ofRectangle & mapRect);
    
    // Possibly add in save and load functions?
    
    bool numberThreshPerPoint(int threshold);
    
    // ofxLeastSquares
    ofxLeastSquares leastSquares;
    
    // number of points that are found
    int inputCount, outputCount;
    float removeOutliersf, lastRemoveOutlier;
    
    // for handling all the raw data processing
    int nDivW, nDivH;
    float xp, yp;                   // x point and y point
    // drawing the regular unscaled map input?
    bool bool_drawError;
    bool bool_drawRawMapInput;
    ofPoint rawDataOffset;
    float rawDataScale;
    
    // have we mapped the pupil yet?
    // if so has it been fitted?
    bool bool_hasFit;
    
    // non temp variables that will be switched
    int currentPoint;
    
    // vector arrays for managed data storage
    // more functionality in cpp than arrays.
    vector<int> ptsInPts;
    vector<ofPoint> leastSquarePoints;
    vector<ofPoint> refPoints;
    vector<ofPoint> screenPoints;
    vector<ofPoint> pupilPoints;
    
    mappingFileManager fm;
};

#endif