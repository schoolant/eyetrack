/**
* Fits the pupil into the screen
* and is responsible for all
* the scaling and data modifications that
* are done when the image is being mapped.

* Data from this was used on my graphs.
*/

#include "fitPupil.h"

// what to do onload
void fitPupil::setup(int _nDivW, int _nDivH) {
    bool_hasFit = false; // have not mapped the pupil yet
    
    nDivW = _nDivW; // flip temp variables
    nDivH = _nDivH;
    
    // fill the vectors with initial data sets
    ptsInPts.assign(nDivW * nDivH, 0);
    refPoints.assign(nDivW * nDivH, ofPoint(0, 0));
    leastSquarePoints.assign(nDivW * nDivH, ofPoint(0, 0));
    
    // number of in/out streams
    inputCount = 6; outputCount = 2;
    
    leastSquares.setup(inputCount, outputCount);
    // init odd maps
    lastRemoveOutlier = 0;
}

// what happens when we start the mapping routine
void fitPupil::startMap() {
    for(int i = 0; i < ptsInPts.size(); i++) {
        ptsInPts[i] = 0; // clear the ptsinpts vector
    }
}

// remove invalid mappings
void fitPupil::removeOutliers() {
    if(bool_hasFit && lastRemoveOutlier != removeOutliersf) {
        leastSquares.resetOutliers();
        leastSquares.removeOutliers(removeOutliersf);
        lastRemoveOutlier = removeOutliersf;
    }
}

// flip variables on component update
void fitPupil::update(int _currentPoint, float _xp, float _yp) {
    currentPoint = _currentPoint;
    xp = _xp;
    yp = _yp;
}

/// SEGFAULT OR ACCESS VIOLATION HERE - WINDOWS ONLY (MinGW)
// The push_backs throw a segfault (on windows) and it needs to be fixed.

// change the current point of the circle on register
// of map input, for advancing positions
void fitPupil::registerMapInput(float x, float y) {
    try{
        //printf("x:%f, y:%f\n", x, y);
        screenPoints.push_back(ofPoint(xp, yp));
        pupilPoints.push_back(ofPoint(x, y));
    } catch(exception &e){
        cout << e.what() << endl;
    }
    ptsInPts[currentPoint]++;
}

// calculate where to draw our point
void fitPupil::calc(ofRectangle & mapRect) {
    if(pupilPoints.size() < 8) return;
    if(numberThreshPerPoint(2))
        
        removePointsOffAverage();
    calcWeight(pupilPoints, screenPoints);
    calcErrors(mapRect);
}

// check the points that are far from avg to see if they
// need to be removed/dropped
// "step" through average points when mapping config is up

// start checking points from the corner to see if false
void fitPupil::removePointsOffAverage() {
    int startPos = 0;
    vector<int> largeErrPoints; // array of points that contain errors
    
    for(int i = 0; i < nDivH; i++) {
        for(int j = 0; j < nDivW; j++) {
            ofPoint tempAvg;
            tempAvg.x = 0;
            tempAvg.y = 0;
            
            for(int k = 0; k < ptsInPts[j + i * nDivW]; k++) {
                tempAvg.x += pupilPoints[k + startPos].x;
                tempAvg.y += pupilPoints[k + startPos].y;
            }
            
            tempAvg /= ptsInPts[j + i * nDivW];
            
            float distAvg = 0;                      // average distance from avg point
            
            for(int k = 0; k < ptsInPts[j + i * nDivW]; k++) {
                distAvg += ofDist(pupilPoints[k + startPos].x, pupilPoints[k + startPos].y, tempAvg.x, tempAvg.y);
            }
            
            distAvg /= ptsInPts[j + i * nDivW];
            
            // based on distance from the points determine if error
            int errorNum = 0;
            
            for(int k = 0; k < ptsInPts[j + i * nDivW]; k++) {
                if(distAvg * 1.7 < ofDist(pupilPoints[k + startPos].x, pupilPoints[k + startPos].y, tempAvg.x, tempAvg.y)) {
                    int temp = k + startPos;
                    largeErrPoints.push_back(temp);
                    errorNum++;
                }
            }
            
            startPos += ptsInPts[j + i * nDivW];
            ptsInPts[j + i * nDivW] -= errorNum;
            
        }
    }
    
    // remove all the error points after finishing removing averages
    for(int i = 0; i < largeErrPoints.size(); i++) {
        pupilPoints.erase(pupilPoints.begin() + largeErrPoints[i] - i);
        screenPoints.erase(screenPoints.begin() + largeErrPoints[i] - i);
    }
    
    startPos = 0;
}

// calculate the steppings and errors in the stepppings of the viewpoint
void fitPupil::calcErrors(ofRectangle & mapRect) {
    int startPos = 0;
    
    for(int i = 0; i < nDivH; i++) {
        for(int j = 0; j < nDivW; j++) {
            int jj;
            
            if(i % 2 == 0)
                jj = nDivW - j - 1;
            else
                jj = j;
            
            refPoints[j + i * nDivW].x = mapRect.x + ((float)mapRect.width / (float)(nDivW - 1)) * jj;
            refPoints[j + i * nDivW].y = mapRect.y + mapRect.height - ((float)mapRect.height / (float)(nDivH)) * i;
            
            ofPoint tempAvg;
            
            tempAvg.x = 0; tempAvg.y = 0;
            
            for(int k = 0; k < ptsInPts[j + i * nDivW]; k++) {
                tempAvg.x += pupilPoints[k + startPos].x;
                tempAvg.y += pupilPoints[k + startPos].y;
            }
            
            tempAvg /= ptsInPts[j + i * nDivW];
            startPos += ptsInPts[j + i * nDivW];
            
            leastSquarePoints[j + i * nDivW] = getMapPoint(tempAvg.x, tempAvg.y);
        }
    }
}

// calculate the weight for the equation that goes from the starting points to the tracking points
void fitPupil::calcWeight(vector<ofPoint> trackedPoints, vector<ofPoint> knownPoints) {
    int length = trackedPoints.size();
    leastSquares.clear();
    
    for(int i = 0; i < length; i++) {
        ofPoint &ipt = trackedPoints[i]; // tracked
        ofPoint &opt = knownPoints[i];   // known
        leastSquares.add(makeIn(ipt.x, ipt.y), makeOut(opt.x, opt.y));
    }
    
    bool_hasFit = true; // pupil has now been fitted!
}

ofPoint fitPupil::getMapPoint(float x, float y) {
    if(bool_hasFit == true) {
        vector<float> out = leastSquares.map(makeIn(x, y));
        
        
        // start clipping things, so the
        // entire program doesnt crash on
        // an array out of bounds error
        float calX = out[0];
        float calY = out[1];
        
        if(calX < -1000)
            calX = -1000;
        
        if(calY < -1000)
            calY = -1000;
        
        if(calX > 2000)
            calX = 2000;
        
        if(calX > 2000)
            calX = 2000;
        
        // make sure things dont get too small
        // else more iterations are needed to solve.
        if(isnan(calX))
            calX = 0;
        if(isnan(calY))
            calY = 0;
        
        return ofPoint(calX, calY);
    }
    
    return ofPoint(0, 0);
}

// is the threshold high enough for each point?
bool fitPupil::numberThreshPerPoint(int threshold) {
    bool bool_highEnoughValue = true; // have a high enough threshold
    // check to see if the (point) threshold is high enough for the points
    for(int i = 0; i < ptsInPts.size(); i++) {
        if(ptsInPts[i] < threshold) {
            bool_highEnoughValue = false; // dont have enough
        }
    }
    return bool_highEnoughValue;
}

// instructions for drawing the mapping
void fitPupil::draw() {
    if(bool_drawError)
        drawError();
    
    // just draw the raw data if conditions are met.
    if(bool_drawRawMapInput)
        drawRawMapInput(rawDataOffset.x, rawDataOffset.y, rawDataScale);
}

// clear points and set mapping to false
void fitPupil::clear() {
    screenPoints.clear();
    pupilPoints.clear();
    bool_hasFit = false;
}

// again drawing a grid
void fitPupil::drawError() {
    if(bool_hasFit) {
        ofSetColor(0, 0, 255);
        // draw on the w*h grid
        for(int i = 0; i < nDivW * nDivH; i++) {
        // draw at increments
            ofLine(refPoints[i].x, refPoints[i].y, leastSquarePoints[i].x, leastSquarePoints[i].y);
        }
    }
    ofSetColor(255, 255, 255);
}

// draw the mapping dot. what subject should look at
void fitPupil::drawRawMapInput(int xOff, int yOff, float scale) {
    if(bool_hasFit) {
        ofSetColor(0, 0, 255);
        for(int i = 0; i < pupilPoints.size(); i++) {
            ofCircle(pupilPoints[i].x * scale + xOff, pupilPoints[i].y * scale + yOff, 3);
        }
    }
    ofSetColor(255, 255, 255); // reset color to white
}

// save and load map settings
void fitPupil::saveMapping() {
    fm.saveCal(leastSquares, nDivW * nDivH, refPoints, leastSquarePoints, pupilPoints, screenPoints);
}

// this prevents recalculation when doing a quick demo
// it also allows for easy data extraction from the xml file
void fitPupil::loadMapping() {
    fm.loadCal(leastSquares, refPoints, leastSquarePoints, pupilPoints, screenPoints);
    bool_hasFit = true; // tell the program that mapping is loaded and completed
}

// draw the rectangle grid to help with aligning the mapping targets
// really just for easier development and looks, not displayed in release
void fitPupil::drawOtherObjects(ofRectangle & mapRect) {
    ofPushStyle();
    ofSetLineWidth(2);
    ofSetColor(255, 0, 0);
    ofNoFill();
    ofRect(mapRect.x, mapRect.y, mapRect.width, mapRect.height);
    ofPopStyle();
}

// Ax + Bx^2 + Cy + Dy^2 + Exy + F
// vector image inputs and outputs
vector<float> fitPupil::makeIn(float x, float y) {
    // ax + bx^2 + cy + dy^2 + exy + f - conic
    vector<float> in;
    in.resize(inputCount);
    in[0] = x;
    in[1] = x * x;
    in[2] = y;
    in[3] = y * y;
    in[4] = x * y;
    in[5] = 1;
    return in;
}

vector<float> fitPupil::makeOut(float x, float y) {
    vector<float> out;
    out.resize(outputCount);
    out[0] = x;
    out[1] = y;
    return out;
}
