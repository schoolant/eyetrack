#include "testApp.h"


void testApp::setup() {
    ofSetVerticalSync(true); // V-SYNC. Thanks to openFrameworks, no tearing!
    
    mode = M_TRACK;
    
    // setup all the components
    trackingHandle.setup(); // tracking
    mapHandle.setup(); // mapping
    
    // mode setups
    mapMouse.setup();
    game.setup();
    kbMode.setup();
    imageAnalysis.setup();
    
    // not currently simulating hardware
    hardwareSimEnabled = false;
    smoothedPupil.set(0, 0, 0); // set raw position
}


void testApp::update() {
    // vim color scheme!
    ofBackground(41, 49, 52);
    
    // update tracking and video
    trackingHandle.update();
    
    // update mapping
    mapHandle.update();
    
    if(mapHandle.bool_auto == true && mapHandle.bool_inAutoMode == true && mapHandle.bool_autoRecording == true) {
        // if a new frame was captured from the camera
        if(trackingHandle.foundEyeInNewFrame()) {
            // assign the raw point
            ofPoint trackedPupil = trackingHandle.givePupilPoint();
            // register our raw data input in the mapping solution
            mapHandle.ptDraw.registerMapInput(trackedPupil.x, trackedPupil.y);
            mapHandle.power = 1;
        }
    }
    
    // if hardware is not being simulated
    if(!hardwareSimEnabled) {
        if(mapHandle.ptDraw.bool_hasFit) {
            ofPoint trackedPupil;
            
            // simulate the mouse hardware
            if(hardwareSimEnabled) {
                trackedPupil.x = mouseX;
                trackedPupil.y = mouseY;
            } else {
                // set the point to the raw eye point
                trackedPupil = trackingHandle.givePupilPoint();
            }
            
            mapHandle.smoothing = 0.7;
            // calculate the mapped points by passing raw data into the mapping function
            ofPoint mappedScreenPoint = mapHandle.ptDraw.getMapPoint(trackedPupil.x, trackedPupil.y);
            // set the mapped points
            smoothedPupil.x = mapHandle.smoothing * smoothedPupil.x + (1 - mapHandle.smoothing) * mappedScreenPoint.x;
            smoothedPupil.y = mapHandle.smoothing * smoothedPupil.y + (1 - mapHandle.smoothing) * mappedScreenPoint.y;
            
            printf("x: %f, y: %f", smoothedPupil.x, smoothedPupil.y);
        }
    }
    else {
        // else, hardware simulation
        smoothedPupil.x = mouseX;
        smoothedPupil.y = mouseY;
    }
    
    // switch updates on mode switches.
    
    // mouse
    if(mode == M_MOUSE) {
        ofPoint pt = smoothedPupil;
        mapMouse.update(pt);
    }
    if(mode == M_KEY) {
        ofPoint pt = smoothedPupil;
        kbMode.update(pt.x, pt.y);
    }
    if(mode == M_IMAGE) {
        ofPoint pt = smoothedPupil;
        imageAnalysis.update(pt.x, pt.y);
    }
    if(mode == M_GAME) {
        ofPoint pt = smoothedPupil;
        game.update(pt.x, pt.y);
    }
}


void testApp::draw() {
    ofSetColor(255, 255, 255);
    
    // draw the correct frames based on the frames
    if(mode == M_TRACK)
        trackingHandle.draw();
    if(mode == M_CAL)
        mapHandle.draw();
    if(mode == M_MOUSE)
        mapMouse.draw();
    if(mode == M_KEY)
        kbMode.draw();
    if(mode == M_IMAGE)
        imageAnalysis.draw();
    if(mode == M_GAME)
        game.draw();
    
    // draw a dot where the pupil is mapped
    if(mapHandle.ptDraw.bool_hasFit || hardwareSimEnabled) {
        // if the mouse is not being plotted...
        if(mode != M_MOUSE) {
            ofSetColor(255, 205, 34, 100);
            ofFill();
            ofCircle(smoothedPupil.x, smoothedPupil.y, 20);
        }
    }
}


void testApp::keyPressed(int key) {
    switch (key) {
        // switch views when 's' is pressed
        case 's':
            // modes and views can be added pretty easily and still
            // use the full functionality of the tracker.
            // however they MUST update on the mapped pupil point.
            mode++;
            mode %= 6; // number of modes
            break;
    }
    
    if(mode == M_CAL)
        mapHandle.keyPressed(key);
    if(mode == M_MOUSE)
        mapMouse.keyPressed(key);
    if(mode == M_IMAGE)
        imageAnalysis.keyPressed(key);
}


void testApp::keyReleased(int key) {
    if(mode == M_MOUSE)
        mapMouse.keyReleased(key);
}


void testApp::mouseMoved(int x, int y ) {
    if(mode == M_MOUSE)
        mapMouse.mouseMoved(x, y);
}


void testApp::mouseDragged(int x, int y, int button) {
    if(mode == M_TRACK)
        trackingHandle.mouseDragged(x, y, button);
    if(mode == M_CAL)
        mapHandle.mouseDragged(x, y, button);
    if(mode == M_MOUSE)
        mapMouse.mouseDragged(x, y, button);
    if(mode == M_IMAGE)
        imageAnalysis.mouseDragged(x, y, button);
}


void testApp::mousePressed(int x, int y, int button) {
    if(mode == M_TRACK)
        trackingHandle.mousePressed(x, y, button);
    if(mode == M_CAL)
        mapHandle.mousePressed(x, y, button);
    if(mode == M_MOUSE)
        mapMouse.mousePressed(x, y, button);
    if(mode == M_IMAGE)
        imageAnalysis.mousePressed(x, y, button);
}


void testApp::mouseReleased(int x, int y, int button) {
    if(mode == M_TRACK)
        trackingHandle.mouseReleased();
    if(mode == M_CAL)
        mapHandle.mouseReleased(x, y, button);
    if(mode == M_MOUSE)
        mapMouse.mouseReleased(x, y, button);
    if(mode == M_IMAGE)
        imageAnalysis.mouseReleased(x, y, button);
}


void testApp::windowResized(int w, int h) {

}


void testApp::gotMessage(ofMessage msg) {

}


void testApp::dragEvent(ofDragInfo dragInfo) { 

}