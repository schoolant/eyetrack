// mouse mode - control the mouse!

// defines for mouseMode.cpp

#pragma once

#ifdef TARGET_OSX // if compiling on osx
#include <ApplicationServices/ApplicationServices.h>
#include <unistd.h>
#endif

#include "ofMain.h"
#include "templateMode.h"

// button - ofxHoverButtons
#include "buttonStateManager.h"
#include "buttonState.h"
#include "buttonRect.h"
#include "buttonToggle.h"
#include "buttonTrigger.h"

class mouseMode : public templateMode {
    public:
    
    mouseMode();
    void setup();
    void draw();
    void update(ofPoint Mouse);
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    
    // left buttons
    void upLeftButton(ofPoint Mouse);
    void downLeftButton(ofPoint Mouse);
    
    // right buttons
    void upRightButton(ofPoint Mouse);
    void downRightButton(ofPoint Mouse);
    
    // mouse drags
    void leftDragged(ofPoint Mouse);
    void rightDragged(ofPoint Mouse);
    
    // click and drags
    bool draggingLeftDown;
    bool draggingRightDown;
    
    // mouse moved
    void move(ofPoint Mouse);
    
    // left button states
    bool bool_leftButton;
    bool getLeftButtonState();
    
    // right button states
    bool bool_rightButton;
    bool getRightButtonState();
    
    #ifdef TARGET_OSX // mouse input on osx
    CGEventSourceRef    source;
    CGPoint             mouseCursorPos;
    
    // event references
    CGEventRef mouseEventMove;
    
    // left
    CGEventRef mouseEventLeftDown;
    CGEventRef mouseEventLeftUp;
    CGEventRef mouseEventLeftDragged;
    
    // right
    CGEventRef mouseEventRightDown;
    CGEventRef mouseEventRightUp;
    CGEventRef mouseEventRightDragged;
    
    // event types
    CGEventType eventTypeMouseMoved;
    
    // left events
    CGEventType eventTypeLeftMouseDown;
    CGEventType eventTypeLeftMouseUp;
    CGEventType eventTypeLeftDragged;
    
    // right events
    CGEventType eventTypeRightMouseDown;
    CGEventType eventTypeRightMouseUp;
    CGEventType eventTypeRightDragged;
    
    #else
    ofPoint mouseCursorPos;
    
    #endif
    
    ofPoint Mouse;
    ofPoint prevMouse;
    
    // for testing the mouse input
    bool bool_mouseMoved;
    int mousex, mousey;
    bool rect1_l, rect2_l, rect1_r, rect2_r;
    float rect_size;
    float rect1_x, rect1_y;
    float rect2_x, rect2_y;
    
    buttonTrigger hoverBtnTrig;
    buttonToggle hoverBtnTog;
    
    ofColor on, off;
    string event;
};