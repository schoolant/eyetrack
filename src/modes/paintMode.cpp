//
//  paintMode.cpp
//  EyeTrack_Updated
//
// Testing image analysis using the pupil point thats
// mapped to a dot that generates a heatmap/drawline in
// real time.

#include "paintMode.h"
#include "groupBin.h"

groupBin groups;

int numButtons;
float rate;
float ptThresh; // point thresholds

void paintMode::setup() {
    ofBackground(255, 255, 255);
    
    // drawing toggles
    bool_drawing = false;
    pause.setup("recording_drawing", "paused", bool_drawing, 10, 10, 130, 75);
    
    // buttons that are avalible when drawing
    nextStroke.setup("next stroke", 10, 175, 130, 60);
    undo.setup("undo stroke", 10, 260, 130, 75);
    showDrawGrid.setup("grid on", "grid off", false, 10, 200, 130, 75);
    
    drawableButtons.push_back(&nextStroke);
    drawableButtons.push_back(&undo);
    
    prefix = save.getValue("session", "eyetrack");
    count  = save.getValue("count", 0);
    
    mx = 0.0;
    my = 0.0;
    
    button.setWaitTime(rate);
    
    if(groups.size() == 0) {
        groups.addGroup();
        groups.back().style.layer = 0;
    }
    
    testState = BUTTON_NONE;
}

void paintMode::drawStroke() {
    if(groups.size()) {
        // try and recycle strokes for a more efficient system
        if(groups.back().strokes.size() && groups.back().getLastStrokeNumPoints() <= 1) {
            groups.back().strokes.back().pts.clear();
        } else {
            // if we can't recycle make a new stroke
            groups.back().begin();
        }
    }
}

void paintMode::update(float mousex, float mousey) {
    nextStroke.setMaxCounter(numButtons);
    undo.setMaxCounter(numButtons);
    pause.setMaxCounter(numButtons);
    showDrawGrid.setMaxCounter(numButtons);
    
    // set the time for buttons to be triggered. universal setting
    button.setWaitTime(rate);
    
    mx = mousex;
    my = mousey;
    
    // update toggle with mouse and get the toggle state
    if(pause.update(mx, my)) {
        bool_drawing = pause.getState();
    }
    
    if(bool_drawing) {
        // update trigger
        if(nextStroke.update(mx, my)) {
            drawStroke();
        }
        
        if(undo.update(mx, my)) {
            groups.back().undo(); // pop the stack, undo.
        }
    } else {
        showDrawGrid.update(mx, my);
    }
    
    bool bool_drawCheck = true;
    if(bool_drawing == false || (testState == BUTTON_NONE && mx < SIDE_GUI_X)) {
        bool_drawCheck = false;
        button.clear();
    }
    
    if(bool_drawCheck) {
        button.update(mx, my);
        bool bool_stopped = button.isPointStationary(ptThresh);
        
        if(bool_stopped) {
            if(testState == BUTTON_NONE) {
                testState == BUTTON_STARTED;
            } else if(testState == BUTTON_STARTED) {
                testState = BUTTON_NONE;
                groups.back().nextStroke();
            }
        }
    }
    
    // check before adding a new segment point
    if(testState == BUTTON_STARTED) {
        // nothing to draw into? add in a group.
        if(groups.size() == 0) {
            groups.addGroup();
        }
        
        // if there are more than 2 points, check if the shape can be closed.
        if(groups.back().getLastStrokeNumPoints() >= 3) {
            ofPoint point = groups.back().strokes.back().pts[0];
            ofPoint mouse(mx, my);
            
            if(point.distance(mouse) <= 12) {
                mx = point.x;
                my = point.y;
            }
        }
        
        // add the point
        groups.back().addPoint(mx, my, ofGetElapsedTimef(), 1.0);
        testState = BUTTON_NONE;
        
    }
}

void paintMode::draw() {
    ofPushStyle();
    
    bool bool_grid = showDrawGrid.getState();
    
    if(bool_grid) {
        float w = ofGetWidth();
        float h = ofGetHeight();
        
        ofPushStyle();
        ofSetLineWidth(1.0);
        ofEnableAlphaBlending();
        ofSetColor(0, 0, 0, 20);
        
        for(int x=SIDE_GUI_X; x<w; x+=20) {
            ofLine(x, 0, x, h);
        }
        
        for(int y=0; y<h; y+=20) {
            ofLine(SIDE_GUI_X, y, w, y);
        }
        
        ofPopStyle();
    }
    
    if(bool_drawing) {
        for(int i=0; i < drawableButtons.size(); i++) {
            drawableButtons[i] -> draw();
        }
        
        if(groups.size() && groups.back().hasPoints() && mx > SIDE_GUI_X) {
            if(!groups.back().bool_newStroke) {
                ofPoint pt = groups.back().getLastPoint();
                
                glLineStipple(1, 0x3F07);
                glEnable(GL_LINE_STIPPLE);
                ofLine(mx, my, pt.x, pt.y);
                glDisable(GL_LINE_STIPPLE);
            }
        }
    } else { // draw the grid
        showDrawGrid.draw();
    }
    
    pause.draw();
    
    if(groups.size()) {
        ofEnableAlphaBlending();
        ofSetColor(0, 0, 0, 30);
        
        for(int i=0; i<groups.size(); i++) {
            groups[i].draw();
        }
        
        ofSetHexColor(0x000000);
        groups.back().draw();
    }
    
    if(mx<SIDE_GUI_X) {
        ofPushStyle();
        ofFill();
        ofSetColor(255, 255, 255);
        ofCircle(mx, my, 9);
        ofSetColor(0, 0, 0);
        ofCircle(mx, my, 6);
        ofPopStyle();
    } else if(testState == BUTTON_NONE) {
        ofNoFill();
        ofCircle(mx, my, 9);
    } else if(testState == BUTTON_STARTED) {
        ofFill();
        ofCircle(mx, my, 5);
    }
    
    ofPopStyle();
}

void paintMode::keyPressed(int key) {
    
}

void paintMode::mouseDragged(int x, int y, int button) {
    
}

void paintMode::mousePressed(int x, int y, int button) {
    
}

void paintMode::mouseReleased(int x, int y, int button) {
    
}