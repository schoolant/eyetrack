#ifndef EyeTrack_Final_strokeBin_h
#define EyeTrack_Final_strokeBin_h

#include "stroke.h"
#include "groupStyles.h"

class strokeBin {
    public:
    
    bool bool_curved, bool_newStroke;
    ofRectangle rect;
    vector<stroke> strokes;
    groupStyles style;
    
    strokeBin() {
        bool_newStroke  = true;
        bool_curved     = false;
    }
    
    void undo() {
        if(strokes.size()) strokes.pop_back();
        bool_newStroke = true;
    }
    
    void undoPoint() {
        if(strokes.size()) {
            strokes.back().undo();
        }
    }
    
    // scale the drawing up or down
    void scale(float sx, float sy) {
        
    }
    
    // rotate the drawing in degrees
    void rotate(float angle) {
        
    }
    
    // translate the drawing
    void shift(float x, float y) {
        
    }
    
    void setIsCurve(bool bcurve) {
        bool_curved = bcurve;
        if(strokes.size()) {
            strokes.back().setCurve(bool_curved);
        }
    }
    
    void begin() {
        bool_newStroke = true;
    }
    
    float getStartTime() {
        if(strokes.size()) {
            if(strokes[0].pts.size()) {
                return strokes[0].pts[0].t;
            }
        }
        
        return 0.0;
    }
    
    void addPoint(float x, float y, float t, float c) {
        if(strokes.size() == 0) {
            bool_newStroke = true;
        }
        
        if(bool_newStroke) {
            strokes.push_back(stroke());
            if(bool_curved) {
                strokes.back().setCurve(bool_curved);
            }
            bool_newStroke = false;
        }
        
        strokes.back().addPoint(x, y, t, c);
        updateBounds();
    }
    
    void nextStroke() {
        bool_newStroke = true;
    }
    
    void clear() {
        strokes.clear();
    }
    
    bool hasPoints() {
        return (strokes.size() && strokes.back().pts.size());
    }
    
    ofPoint getLastPoint() {
        if(hasPoints()) {
            return (ofPoint)(strokes.back().pts.back());
        }
    }
    
    int getLastStrokeNumPoints() {
        int num = 0;
        if(strokes.size())
            return strokes.back().pts.size();
        return num;
    }
    
    int getTotalNumPoints() {
        int num = 0;
        for(int j=0; j<strokes.size(); j++) {
            num += strokes[j].pts.size();
        }
        return num;
    }
    
    void updateBounds() {
        ofRectangle boundsRect;
        
        float min_x, min_y, max_x, max_y;
        
        for(int i=0; i<strokes.size(); i++) {
            if(!strokes[i].hasPoints()) continue;
            rect = strokes[i].getBounds();
            
            if(rect.width == 0 || rect.height == 0) continue;
            
            if(i==0) {
                min_x = rect.x;
                min_y = rect.y;
                max_x = rect.width + min_x;
                max_y = rect.height + min_y;
            } else { // clipping stuffs for accurate bounds when drawing
                if(rect.x < min_x) min_x = rect.x;
                if(rect.x + rect.width > max_x) max_x = rect.x + rect.width;
                if(rect.y < min_y) min_y = rect.y;
                if(rect.y + rect.height > max_y) max_y = rect.y + rect.height;
            }
        }
        
        rect = ofRectangle(min_x, min_y, max_x-min_x, max_y-min_y);
    }
    
    ofRectangle getBounds() {
        return rect;
    }
    
    void drawBounds() {
        updateBounds();
        ofPushStyle();
            ofNoFill();
            ofRect(rect.x, rect.y, rect.width, rect.height);
        ofPopStyle();
    }
    
    void draw(bool lcircle = true) {
        for(int i=0; i<strokes.size(); i++) {
            strokes[i].draw(lcircle);
        }
    }
};

#endif
