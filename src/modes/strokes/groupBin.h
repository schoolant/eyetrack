// groupBin.h
// provides group collections storage
// in a "bin" format

#ifndef EyeTrack_Final_groupBin_h
#define EyeTrack_Final_groupBin_h

#include "ofMain.h"
#include "strokeBin.h"

class groupBin {
    protected:
    vector<strokeBin> groups;
    //-----------------------
    
    public:
    
    // vector inherited methods
    void clear() {
        groups.clear();
    }
    
    strokeBin & operator[](const unsigned int i) {
        if(groups.size() == 0) {
            addGroup();
        }
        
        int index = ofClamp(i, 0, groups.size());
        return groups[index];
    }
    
    unsigned int size() {
        return groups.size();
    }
    
    strokeBin & back() {
        if(groups.size() == 0) {
            addGroup();
        }
        
        return groups.back();
    }
    
    vector<strokeBin> & getVector() {
        return groups;
    }
    
    // for strokeBin
    void addGroup() {
        groups.push_back(strokeBin());
    }
    
    bool hasPoints() {
        for(int i=0; i<groups.size(); i++) {
            if(groups[i].hasPoints()) return true;
        }
        return false;
    }
};

#endif
