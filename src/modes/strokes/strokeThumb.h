#ifndef EyeTrack_Final_strokeThumb_h
#define EyeTrack_Final_strokeThumb_h

#include "strokeBin.h"

class strokeThumb {
    
    public:
    
    void drawThumbnails(ofRectangle drawRect, float space, vector<strokeBin> thumbs);
    
};

#endif
