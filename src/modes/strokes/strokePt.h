#ifndef EyeTrack_Final_strokePt_h
#define EyeTrack_Final_strokePt_h

#include "ofMain.h"

class strokePt : public ofPoint {
    public:
    
    strokePt(float _x, float _y, float _t, float _conf){
            x = _x;
            y = _y;
            t = _t;
            conf = _conf;
    }
    
    float conf, t;
    
};

#endif
