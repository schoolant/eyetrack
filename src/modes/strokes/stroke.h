#ifndef EyeTrack_Final_stroke_h
#define EyeTrack_Final_stroke_h

#include "ofMain.h"
#include "strokePt.h"

class stroke {
    public:
    
    bool bool_curve;
    
    vector<strokePt> pts;
    ofRectangle rect;
    
    stroke() {
        bool_curve = false;
    }
    
    void addPoint(float x, float y, float t, float conf = 1.0) {
        pts.push_back(strokePt(x, y, t, conf));
        updateBounds();
    }
    
    void undo() {
        if(pts.size()) {
            pts.pop_back();
            updateBounds();
        }
    }
    
    void setCurve(bool bc) {
        bool_curve = bc;
    }
    
    bool hasPoints() {
        return (pts.size() > 0);
    }
    
    void updateBounds() {
        if(pts.size() <= 0) {
            rect = ofRectangle();
        } else if(pts.size()==1) {
            rect = ofRectangle(pts[0].x, pts[0].y, 1, 1);
            return;
        }
        
        float min_x = 9999999.0f;
        float min_y = 9999999.0f;
        float max_x = -999999.0f;
        float max_y = -999999.0f;
    
        for(int i=0; i < pts.size(); i++){
            if( pts[i].x < min_x ) min_x = pts[i].x;
            if( pts[i].x > max_x ) max_x = pts[i].x;
            if( pts[i].y < min_y ) min_y = pts[i].y;
            if( pts[i].y > max_y ) max_y = pts[i].y;
        }
    
        rect = ofRectangle(min_x, min_y, max_x-min_x, max_y-min_y);
    
    }
    
    ofRectangle getBounds() {
        return rect;
    }
    
    void draw(bool bool_lcircle = true) {
        ofPushStyle();
        
        if(pts.size()) {
            if(bool_curve && pts.size() >= 2) {
                ofNoFill();
                ofBeginShape();
                    ofCurveVertex(pts[0].x, pts[0].y);
                    for(int i=0; i<pts.size(); i++) {
                        ofCurveVertex(pts[i].x, pts[i].y);
                    }
                    ofCurveVertex(pts.back().x, pts.back().y);
                ofEndShape();
            } else {
                ofNoFill();
                ofBeginShape();
                    for(int i=0; i<pts.size(); i++) {
                        ofVertex(pts[i].x, pts[i].y);
                    }
                ofEndShape();
            }
        }
        
        if(bool_lcircle) {
            ofFill();
            if(pts.size()) {
                ofCircle(pts.back().x, pts.back().y, 4);
            }
        }
        ofPopStyle();
    }
};

#endif
