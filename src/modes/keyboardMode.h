//
//  keyboardMode.h
//  EyeTrack_Updated
//
// defines for keyboardMode.cpp

#ifndef EyeTrack_Updated_keyboardMode_h
#define EyeTrack_Updated_keyboardMode_h

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "templateMode.h"

// ofxHoverRect
#include "buttonRect.h"
#include "buttonToggle.h"
#include "buttonTrigger.h"

class keyboardMode : public templateMode {
    public:
    
    void setup();
    void update(float mousex, float mousey);
    void draw();
    void logMessageHist();

    buttonTrigger doneBtn;                          // done

    vector<buttonTrigger>   letterButtons;          // all the keys, letters only
                                                    // defined as triggerbuttons
    vector<buttonToggle> actionButtons;
    vector<string>          lower_letterButtons;    // lowercase keys
    vector<string>          trigMessageLog;         // triggerd input message log
    vector<int>             trigKeyCodes;           // triggered key codes
    string *buttons;
    string dispMessage;                             // message typed
    int tabCounter;                                 // count '/t'
    int buttonCount;

    bool bool_shift;

    // selected the source code pro font
    ofTrueTypeFont sourceCodePro;
    ofTrueTypeFont sourceCodeProLight;
    ofTrueTypeFont sourceCodeProBold;
    //ofTrueTypeFont verdana;

    private:

    void messageTyped(string msg);
};

#endif
