//
//  paintMode.h
//  EyeTrack_Updated
//
// Load an image and draw over it based on the
// blob that is responsible for pupil tracking
// working on heatmap development, so at somepoint
// there will be heatmaps overlaying ofter drawing

#ifndef EyeTrack_Updated_paintMode_h
#define EyeTrack_Updated_paintMode_h

#include "ofMain.h"
#include "defines.h"
#include "ofxXmlSettings.h"
#include "ofxControlPanel.h"
#include "templateMode.h"
#include "strokeBin.h"

// button - ofxHoverButtons
#include "buttonStateManager.h"
#include "buttonState.h"
#include "buttonRect.h"
#include "buttonToggle.h"
#include "buttonTrigger.h"

// handles image editing and rendering
#include "ofxOpenCv.h"

class paintMode : public templateMode {
public:
    
    void setup();
    void update(float mousex, float mousey);
    void draw();
    
    void drawStroke();
    
    buttonState         testState;
    buttonStateManager  button;
    
    // the next stroke to make
    buttonTrigger       nextStroke;
    
    // show a draw grid for ease of drawing with the eye
    buttonToggle        showDrawGrid;
    buttonToggle        pause; // pause the strokes/drawing
    
    buttonTrigger       undo;
    buttonTrigger       toggleCurves;
    
    // are we drawing curves now?
    bool bool_curves;
    bool bool_drawing; // are we drawing?
    
    int     count;
    string  prefix;
    
    ofxXmlSettings save;
    ofxXmlSettings settings;
    
    float tagTimeBegin;
    
    void keyPressed(int key);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    
};

#endif
