//
//  gameMode.cpp
//  EyeTrack_Updated
//
// small little pong game that was thrown
// together on the car ride to region scifair
// works pretty well to demonstrate the "fun" of
// tracking eyes...

#include "gameMode.h"

extern int buttonCount;

void gameMode::setup() {
    ballRad = 30;
    ballPos.set(20,20);
    ballVelocity.set(ofRandom(-5, 5), ofRandom(2, 9));
    
    // create the two buttons, 100x100
    speedUp.setup("Speed Up", ofGetWidth() - 220, 10, 100, 100);
    speedUp.setRetrigger(true); // can be retriggered
    
    slowDown.setup("Slow down", ofGetWidth() - 110, 10, 100, 100);
    slowDown.setRetrigger(true);
}

void gameMode::update(float mousex, float mousey) {
    float mx = mousex; // super perms
    float my = mousey;
    
    // if the button has been triggered
    if(speedUp.update(mx, my)) {
        ballVelocity.x *= 1.5;
        ballVelocity.y *= 1.5;
    }
    
    if(slowDown.update(mx, my)) {
        ballVelocity.x /= 1.5;
        ballVelocity.y /= 1.5;
    }
    
    float speed = 60.0f / MAX(ofGetFrameRate(), 5); // fps
    
    ballPos.x += ballVelocity.x * speed;
    ballPos.y += ballVelocity.y * speed;
    
    
    // setting up a "wall" so the ball cannot leave the window
    
    // x
    if(ballPos.x < ballRad) {
        ballPos.x = ballRad;
        ballVelocity.x *= -1;
    }
    if(ballPos.x > ofGetWidth() - ballRad) {
        ballPos.x = ofGetWidth() - ballRad;
        ballVelocity.x *= -1;
    }
    
    // y
    if(ballPos.y < ballRad) {
        ballPos.y = ballRad;
        ballVelocity.y *= -1;
    }
    // factoring in the paddle
    if(ballPos.y > ofGetHeight() - 95) {
        float diff = rectPoints.x - ballPos.x;
        if(fabs(diff)<110/2) {
            ballPos.y = ofGetHeight() - 95;
            ballVelocity.y *= -1;
        }
    }
    
    if(ballPos.y > ofGetHeight()) {
        ballPos.x = ofRandom(0, ofGetWidth());
        ballPos.y = 20;
        ballVelocity.set(ofRandom(-5, 5), ofRandom(2, 9));
    }
    
    rectPoints.x = mx;
    rectPoints.y = ofGetHeight() - 40;
}

void gameMode::draw() {
    ofPushStyle();
    
    ofFill();
    ofSetColor(255, 255, 255);
    ofCircle(ballPos.x, ballPos.y, ballRad);
    
    ofSetColor(255);
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofRect(rectPoints.x, rectPoints.y, 110, 20);
    ofSetRectMode(OF_RECTMODE_CORNER);
    
    ofPopStyle();
    
    speedUp.draw(255);
    slowDown.draw(255);
}