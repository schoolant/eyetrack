#include <iostream.h>
#include "mouseMode.h"

// http://stackoverflow.com/questions/2734117/simulating-mouse-input-programmatically-in-os-x

// this is a pretty cool addition, and the one of the main 
// applications for this project that I used in a lot of my
// research. Lets you control the mouse with your eye.

// reason for such a huge size is all the OSX event
// handlers take up a lot of filespace and memory, 
// however there is not really a simple way around not using them

mouseMode::mouseMode() {
    
}

void mouseMode::setup() {
    // init booleans and set positions to false
    bool_leftButton = false;
    bool_rightButton = false;
    draggingLeftDown = false;
    draggingRightDown = false;
    
    // mouse hover buttons - toggle and trigger
    hoverBtnTrig.setup("Hover", 80, 350, 120, 120);
    hoverBtnTog.setup("On", "Off", false, 200, 200, 120, 120);
    
    #ifdef TARGET_OSX
    eventTypeMouseMoved     = kCGEventMouseMoved;
    
    // left
    eventTypeLeftDragged    = kCGEventLeftMouseDragged;
    eventTypeLeftMouseDown  = kCGEventLeftMouseDown;
    eventTypeLeftMouseUp    = kCGEventLeftMouseUp;
    
    // right
    eventTypeRightDragged    = kCGEventRightMouseDragged;
    eventTypeRightMouseDown  = kCGEventRightMouseDown;
    eventTypeRightMouseUp    = kCGEventRightMouseUp;
    #endif
    
    // testing rects
    rect_size = 100; // 100^2
    // 1
    rect1_x = ofRandom(rect_size, ofGetWidth() - rect_size);
    rect1_y = ofRandom(rect_size, ofGetHeight() - rect_size);
    // 2
    rect2_x = ofRandom(rect_size, ofGetWidth() - rect_size);
    rect2_y = ofRandom(rect_size, ofGetHeight() - rect_size);
    
    // clicks?
    rect1_l = false;
    rect2_l = false;
    
    rect1_r = false;
    rect2_r = false;
    
    // color
    on.r = 0; on.g = 0; on.b = 255;
    off.r = 255; off.g = 0; off.b = 0;
    
    Mouse.x = ofGetWidth() / 2;
    Mouse.y = ofGetHeight() / 2;
}

// just moving the mouse
void mouseMode::update(ofPoint Mouse) {
    if(hoverBtnTrig.update(Mouse.x + 10, Mouse.y + 10)) {
        hoverBtnTrig.x = ofRandom(120, ofGetWidth() - 120);
        hoverBtnTrig.y = ofRandom(120, ofGetHeight() - 120);
    } else if(hoverBtnTog.update(Mouse.x + 10, Mouse.y + 10)) {
        hoverBtnTog.x = ofRandom(120, ofGetWidth() - 120);
        hoverBtnTog.y = ofRandom(120, ofGetHeight() - 120);
    }

    // if the mouse has moved run move()
    if(Mouse.x != prevMouse.x || Mouse.y != prevMouse.y) {
        bool_mouseMoved = true;
        prevMouse = Mouse;
    } else {
        bool_mouseMoved = false;
    }
    
    if(bool_mouseMoved) {
        move(Mouse);
    }
}

// draw all the mouse stuff
void mouseMode::draw() {
    hoverBtnTrig.draw();
    hoverBtnTog.draw();

    ofSetColor(255);
    ofDrawBitmapString("", rect1_x, rect1_y - 10);
    ofDrawBitmapString("", rect2_x, rect2_y - 10);
    ofDrawBitmapString(event, ofGetWidth() / 2+30, 50);
    
    ofSetColor(off);
    if(rect1_l)
        ofSetColor(on);
    else if(rect1_r)
        ofSetColor(100, 255, 100);
    ofRect(rect1_x, rect1_y, rect_size, rect_size);
    
    ofSetColor(off);
    if(rect2_l)
        ofSetColor(on);
    else if(rect2_r)
        ofSetColor(100, 255, 100);
    ofRect(rect2_x, rect2_y, rect_size, rect_size);
}

void mouseMode::keyPressed(int key) {
    switch (key) {
        case OF_KEY_LEFT:
            Mouse.x -= 10;
            if(getLeftButtonState())
                leftDragged(Mouse);
            else
                move(Mouse);
            break;
            
        case OF_KEY_RIGHT:
            Mouse.x += 10;
            if(getLeftButtonState())
                leftDragged(Mouse);
            else
                move(Mouse);
            break;
            
        case OF_KEY_UP:
            Mouse.y -= 10;
            if(getLeftButtonState())
                leftDragged(Mouse);
            else
                move(Mouse);
            break;
            
        case OF_KEY_DOWN:
            Mouse.y += 10;
            if(getLeftButtonState())
                leftDragged(Mouse);
            else
                move(Mouse);
            break;
        
        case '1':
            downLeftButton(Mouse);
            break;
        
        case '2':
            downRightButton(Mouse);
            break;
    }
}

void mouseMode::keyReleased(int key) {
    switch (key) {
        case '1':
            upLeftButton(Mouse);
            break;
        
        case '2':
            upRightButton(Mouse);
            break;
    }
}

// set event to moved
void mouseMode::mouseMoved(int x, int y) {
    event = "move x: "+ofToString(x)+" y: "+ofToString(y);
}

void mouseMode::mouseDragged(int x, int y, int button) {
    if(button == 0) {
        event = "left dragged x: "+ofToString(x)+" y: "+ofToString(y);
    
        if(rect1_l) {
            rect1_x = x - rect_size / 2;
            rect1_y = y - rect_size / 2;
        } else if (rect2_l) {
            rect2_x = x - rect_size / 2;
            rect2_y = y - rect_size / 2;
        }
    }
}

// get mouse presses and determine if they're in the rect
void mouseMode::mousePressed(int x, int y, int button) {
    if(button == 0) {
        event = "left button x: "+ofToString(Mouse.x)+" y: "+ofToString(Mouse.y);
        if(x>rect1_x && x<rect1_x+rect_size && y>rect1_y && y<rect1_y+rect_size) {
            rect1_l = true;
        } else {
            rect1_l = false;
        }
        
        if(x>rect2_x && x<rect2_x+rect_size && y>rect2_y && y<rect2_y+rect_size) {
            rect2_l = true;
        } else {
            rect2_l = false;
        }
    } else if(button == 2) {
        event = "right button x: "+ofToString(Mouse.x)+" y: "+ofToString(Mouse.y);
        if(x>rect1_x && x<rect1_x+rect_size && y>rect1_y && y<rect1_y+rect_size) {
            rect1_r = true;
        } else {
            rect1_r = false;
        }
        
        if(x>rect2_x && x<rect2_x+rect_size && y>rect2_y && y<rect2_y+rect_size) {
            rect2_r = true;
        } else {
            rect2_r = false;
        }
    }
}

void mouseMode::mouseReleased(int x, int y, int button) {
    if(button == 0) {
        rect1_l = false;
        rect2_l = false;
    } else if(button == 2) {
        rect1_r = false;
        rect2_r = false;
    }
}

// starting mouse control
// move
void mouseMode::move(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    mouseEventMove = CGEventCreateMouseEvent(CGEventSourceCreate(NULL), eventTypeMouseMoved, mouseCursorPos, kCGMouseButtonLeft);
    CGEventSetType(mouseEventMove, kCGEventMouseMoved);
    CGEventPost(kCGSessionEventTap, mouseEventMove);
    CFRelease(mouseEventMove);
    #endif
}

// left buttons

void mouseMode::downLeftButton(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonLeft;
    mouseEventLeftDown = CGEventCreateMouseEvent(CGEventSourceCreate(NULL), eventTypeLeftMouseDown, mouseCursorPos, kCGMouseButtonLeft);
    CGEventSetType(mouseEventLeftDown, kCGEventLeftMouseDown);
    CGEventPost( kCGSessionEventTap, mouseEventLeftDown );
    CFRelease(mouseEventLeftDown);
    #endif
    bool_leftButton = true;
}

void mouseMode::upLeftButton(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonLeft;
    mouseEventLeftUp = CGEventCreateMouseEvent (CGEventSourceCreate(NULL), eventTypeLeftMouseDown, mouseCursorPos, kCGMouseButtonLeft);
    CGEventSetType(mouseEventLeftUp, kCGEventLeftMouseUp);
    CGEventPost( kCGSessionEventTap, mouseEventLeftUp );
    CFRelease(mouseEventLeftUp);
    bool_leftButton = false;
    
    if(draggingLeftDown){
        CFRelease(mouseEventLeftDragged);
        draggingLeftDown = false;
    }
    #endif
}

bool mouseMode::getLeftButtonState() {
    return bool_leftButton;
}

// right buttons

void mouseMode::downRightButton(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonRight;
    mouseEventRightDown = CGEventCreateMouseEvent (CGEventSourceCreate(NULL), eventTypeRightMouseDown, mouseCursorPos, kCGMouseButtonRight);
    CGEventSetType(mouseEventRightDown, kCGEventRightMouseDown);
    CGEventPost( kCGSessionEventTap, mouseEventRightDown );
    CFRelease(mouseEventRightDown);
    #endif
    
    bool_rightButton = true;
}

void mouseMode::upRightButton(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonRight;
    mouseEventRightUp = CGEventCreateMouseEvent (CGEventSourceCreate(NULL), eventTypeRightMouseUp, mouseCursorPos, kCGMouseButtonRight);
    CGEventSetType(mouseEventRightUp, kCGEventRightMouseUp);
    CGEventPost( kCGSessionEventTap, mouseEventRightUp );
    CFRelease(mouseEventRightUp);
    bool_rightButton = false;
    
    if(draggingRightDown){
        CFRelease(mouseEventRightDragged);
        draggingRightDown = false;
    }
    #endif
}

bool mouseMode::getRightButtonState() {
    return bool_rightButton;
}

// drag

void mouseMode::leftDragged(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonLeft;
    mouseEventLeftDragged = CGEventCreateMouseEvent(CGEventSourceCreate(NULL), eventTypeLeftDragged, mouseCursorPos, kCGMouseButtonLeft);
    CGEventSetType(mouseEventLeftDragged, kCGEventLeftMouseDragged);
    CGEventPost(kCGSessionEventTap, mouseEventLeftDragged);
    #endif
    draggingLeftDown = true;
}

void mouseMode::rightDragged(ofPoint Mouse) {
    mouseCursorPos.x = Mouse.x;
    mouseCursorPos.y = Mouse.y;
    
    #ifdef TARGET_OSX
    CGMouseButton mouseButton = kCGMouseButtonRight;
    mouseEventRightDragged = CGEventCreateMouseEvent (CGEventSourceCreate(NULL), eventTypeRightDragged, mouseCursorPos, kCGMouseButtonRight);
    CGEventSetType(mouseEventRightDragged, kCGEventRightMouseDragged);
    CGEventPost( kCGSessionEventTap, mouseEventRightDragged );
    #endif
    draggingRightDown = true;
}














