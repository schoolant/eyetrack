// template mode
// base for all the modes that includes virtual methods

#ifndef EyeTrack_Updated_templateMode_h
#define EyeTrack_Updated_templateMode_h

#include "ofMain.h"
#include "defines.h"
#include "buttonRect.h"

class templateMode {
    public:
    
    virtual void setup() {}
    virtual void update(float mousex, float mousey) {}
    virtual void draw() {}
    
    // so we can draw buttons with ease
    vector<buttonRect *> drawableButtons;
    
    // mouse x, mouse y
    float mx, my;
};

#endif