//
//  gameMode.h
//  EyeTrack_Updated
//
// defines the functions and variables used
// in the gameMode.cpp file

#ifndef EyeTrack_Updated_gameMode_h
#define EyeTrack_Updated_gameMode_h

// buttons - ofxHoverButtons
#include "buttonStateManager.h"
#include "buttonState.h"
#include "buttonRect.h"
#include "buttonToggle.h"
#include "buttonTrigger.h"

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "templateMode.h"

class gameMode : public templateMode {
    public:
  
    void setup();
    void update(float mousex, float mousey);
    void draw();
    void colorize();
  
    int ballRad;
  
    ofPoint rectPoints;
    ofPoint ballPos;
    ofPoint ballVelocity;
  
    buttonTrigger speedUp;
    buttonTrigger slowDown;
  
};

#endif
