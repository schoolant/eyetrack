//
//  keyboardMode.cpp
//  EyeTrack_Updated
//
// This lets the user control an ENTIRE (letter based) keyboard with their eye!
// right now the updates are a bit too fast and sometimes overflow of text occours in the 
// lower boxes of the "display"

// uses similar ideas of the mouseMode, except this time I'm using arrays instead
// of the OSX listeners, everything is draw and managed with arrays, besides the
// order of letters and numbers.

#include "testApp.h" // ref main app loader header
#include "keyboardMode.h"

void keyboardMode::setup() {

    // load all the fonts from the reference folder
    sourceCodePro.loadFont("../../src/modes/font_resource/fontRegular.ttf", 32); // or 32 fontsize?
    sourceCodeProLight.loadFont("../../src/modes/font_resource/fontLight.ttf", 16);
    tabCounter = 0;
    
    ofBackground(255, 255, 255);
    bool_shift = false;
    
    // "key" button setup map - QWERTY
    
    // ^ - light
    // 6 - regular
    
    // define the keyboard
    string buttons[36] =
    {
        "!\n1", "@\n2", "#\n3", "$\n4", "%\n5", "^\n6", "&\n7", "*\n8", "(\n9", ")\n10",
        "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
        "A", "S", "D", "F", "G", "H", "J", "K", "L",
        "Z", "X", "C", "V", "B", "N", "M"
    };
    
    // get the button sensitivity
    buttonCount = ((testApp *)ofGetAppPtr()) -> buttonSensitivity;
    
    // kb: startpos
    float startX = 5;
    float startY = 5;
    
    // kb: w, h
    float boardW = ofGetHeight() / 8;
    float boardH = ofGetHeight() / 8;
    
    // offsettings
    float add2X = 5;
    float add2Y = 5;
    
    // get the keys ready for drawing
    for(int i = 0; i < 36; i++) {
        buttonTrigger numButton;
        numButton.setup(buttons[i], startX, ofGetHeight() / 5 + startY, boardW, boardH);
        numButton.setMaxCounter(buttonCount);
        numButton.setRetrigger(true);
        
        // possible segfault on MinGW?
        letterButtons.push_back(numButton);
        startX += boardW + add2X;
        
        if(startX > ofGetWidth() - boardW) {
            startX = 5;
            startY += boardH + add2Y;
        }
    }
    
    // for quick debug clipping
    float addX = 5;
    float addY = 105;
    
    // return the y value of the last button
    float lowerMarg = letterButtons[35].getY();
    
    // capslock - had some extra time...
    buttonToggle capsBtn;
    capsBtn.setup("CAPS ON", "CAPS OFF", false, addX, ofGetHeight() - addY * 2, boardW, boardH);
    capsBtn.setMaxCounter(buttonCount);
    
    // assign the caps lock button to the action buttons
    actionButtons.push_back(capsBtn);
    
    // space button
    buttonTrigger spaceBtn;
    spaceBtn.setup("SPACE", (boardW*6) + addX*7, ofGetHeight() - (boardH + addX), boardW*4 + addX*3, boardH);
    spaceBtn.setMaxCounter(buttonCount);
    spaceBtn.setRetrigger(false);
    letterButtons.push_back(spaceBtn);
    
    // delete string
    buttonTrigger delAllBtn;
    delAllBtn.setup("DELETE ALL", (boardW*2) + addX*2, ofGetHeight() - (boardH + addX), boardW*2 + addX, boardH);
    delAllBtn.setMaxCounter(buttonCount);
    delAllBtn.setRetrigger(false);
    letterButtons.push_back(delAllBtn);
    
    // assing all the numbers
    // 0-10
    lower_letterButtons.push_back("0");
    lower_letterButtons.push_back("1");
    lower_letterButtons.push_back("2");
    lower_letterButtons.push_back("3");
    lower_letterButtons.push_back("4");
    lower_letterButtons.push_back("5");
    lower_letterButtons.push_back("6");
    lower_letterButtons.push_back("7");
    lower_letterButtons.push_back("8");
    lower_letterButtons.push_back("9");
    
    // assign all the letters in the letterbutton array
    // qwerty
    lower_letterButtons.push_back("q");
	lower_letterButtons.push_back("w");
	lower_letterButtons.push_back("e");
	lower_letterButtons.push_back("r");
	lower_letterButtons.push_back("t");
	lower_letterButtons.push_back("y");
	lower_letterButtons.push_back("u");
	lower_letterButtons.push_back("i");
	lower_letterButtons.push_back("o");
	lower_letterButtons.push_back("p");
	lower_letterButtons.push_back("a");
	lower_letterButtons.push_back("s");
	lower_letterButtons.push_back("d");
	lower_letterButtons.push_back("f");
	lower_letterButtons.push_back("g");
	lower_letterButtons.push_back("h");
	lower_letterButtons.push_back("j");
	lower_letterButtons.push_back("k");
	lower_letterButtons.push_back("l");
	lower_letterButtons.push_back("z");
	lower_letterButtons.push_back("x");
	lower_letterButtons.push_back("c");
	lower_letterButtons.push_back("v");
	lower_letterButtons.push_back("b");
	lower_letterButtons.push_back("n");
	lower_letterButtons.push_back("m");
    
    
    for(int i = 0; i < letterButtons.size(); i++) {
        letterButtons[i].setDisplayFont(&sourceCodeProLight);
    }
    
    mx = 0.0;
    my = 0.0;
}

void keyboardMode::update(float mousex, float mousey) {
    mx = mousex;
    mx = mousey;
    
    for(int i = 0; i < letterButtons.size(); i++) {
        letterButtons[i].setMaxCounter(buttonCount);
        if(letterButtons[i].update(mousex, mousey)) {
            if((tabCounter == 32) || (tabCounter == 31)) {
                
            // handle all of the display/action key presses
            } else if(letterButtons[i].displayText == "SPACE") {
                dispMessage.push_back(' ');
                tabCounter++;
            } else if(letterButtons[i].displayText == "DELETE ALL") {
                dispMessage.clear();
                tabCounter = 0;
            } else if(bool_shift && (letterButtons[i].displayText == ")\n0")) {
                dispMessage.push_back(')');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "(\n9")) {
                dispMessage.push_back('(');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "*\n8")) {
                dispMessage.push_back('*');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "&\n7")) {
                dispMessage.push_back('&');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "^\n6")) {
                dispMessage.push_back('^');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "%\n5")) {
                dispMessage.push_back('%');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "$\n4")) {
                dispMessage.push_back('$');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "#\n3")) {
                dispMessage.push_back('#');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "@\n2")) {
                dispMessage.push_back('@');
                tabCounter++;
            } else if(bool_shift && (letterButtons[i].displayText == "!\n1")) {
                dispMessage.push_back('!');
                tabCounter++;
                // else handle all the other letters
            } else {
                // if caps/shift is on
                if(bool_shift) {
                    dispMessage.push_back(letterButtons[i].displayText.c_str()[0]);
                } else {
                    dispMessage.push_back(lower_letterButtons[i].c_str()[0]);
                }
            }
        }
    }
    
    // turn on/off caps lock by switching it
    for(int i = 0; i < actionButtons.size(); i++) {
        actionButtons[i].setMaxCounter(buttonCount);
        if(actionButtons[i].update(mousex, mousey)) {
            if(actionButtons[i].displayText[1] == "CAPS ON") {
                if(bool_shift) bool_shift = false;
                else bool_shift = true;
            }
        }
    }
}

// I should draw the keys on a grid, so it looks like a keyboard
void keyboardMode::draw() {
    ofPushStyle();
    
    // draw all the letter buttons and action buttons
    for(int i = 0; i < letterButtons.size(); i++) {
        letterButtons[i].draw();
    }
    for(int i = 0; i < actionButtons.size(); i++) {
        actionButtons[i].draw();
    }
    
    ofFill();
    
    // draw the messagebox area
    ofSetColor(0, 0, 0);
    ofRect(0, 0, ofGetWidth(), ofGetHeight() / 5); // msgbox area
    
    ofSetColor(255, 255, 255); // set the string color
    string lOutMsg = ""; // set the default message to blank
    
    // print the message that is stored in the array
    if(dispMessage.size() > 0) {
        int c = 0;
        for(int i = 0; i < dispMessage.size(); i++) {
            // set the inputted message to the display message
            lOutMsg.push_back(dispMessage[i]);
            c++;
            // max chars the box can hold is 57
            if(c > 58) {
                // so I'll clear it
                lOutMsg += "-\n";
                c = 0;
            }
        }
    }
    
    // draw the string in the messagebox using the
    // Adobe Source Code Pro font.
    sourceCodePro.drawString(lOutMsg, 30, 50);
    
    
    // sub messages to prevent [complete] overflow
    vector<string> strings = ofSplitString(lOutMsg, "\n");
    string subMsg = lOutMsg;
    
    if(strings.size() > 1)
        subMsg = strings[strings.size() - 1];
    
    // get widths and heights of the subbox
    float xx = sourceCodePro.getStringBoundingBox(subMsg + ".", 30, 50).x + sourceCodePro.getStringBoundingBox(subMsg + ".", 30, 50).width;
    float yy = sourceCodePro.getStringBoundingBox(lOutMsg + ".", 30, 50).y + sourceCodePro.getStringBoundingBox(lOutMsg + ".", 30, 50).height;
    
    ofSetColor(127, 127, 127);
    ofRect(xx, yy, 10, 3);
    
    ofPopStyle();
}















