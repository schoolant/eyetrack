#ifndef EyeTrack_Final_groupStyles_h
#define EyeTrack_Final_groupStyles_h

#include <string>
#include <map>

using namespace std;

class groupStyles {
    public:
    
    groupStyles(){init(0);}
    groupStyles(int l){init(l);}
    virtual ~groupStyles(){};
    
    int layer;
    int outline;
    
    bool bool_dotShadowed;
    bool bool_shadowHash;
    bool bool_fill;
    bool bool_holes;
    
    map<string, int> colors;
    map<string, int> atts;
    
    void init(int l) {
        colors["fillColor"]     = 0xffffff;
        colors["shadowColor"]   = 0xffffff;
        colors["strokeColor"]   = 0x000000;
        colors["outlineColor"]  = 0xaaaaaa;
        
        atts["strokeWeight"]    = 2;
        atts["dropShadow_x"]    = 0;
        atts["dropShadow_y"]    = 0;
        atts["outline"]         = 0;
        atts["brushId"]         = 0;
        
        outline     = false;
        layer       = l;
        
        bool_dotShadowed    = false;
        bool_shadowHash     = false;
        bool_fill           = false;
        bool_holes          = false;
        
    }

};

#endif
