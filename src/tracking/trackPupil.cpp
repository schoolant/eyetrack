//
//  trackPupil.cpp
//  EyeTrack_Updated
//
// This is the most important file (besides main of course)
// that handles all of the calculations for solving the conic ellipse
// and plotting a pupil. If you read through this you will see that its
// composed of 2 different classes and multiple boolean methods that each
// trigger something different. The drawing is done in the header.

#include "trackPupil.h"
#include "ofxPoint2f.h"
#include "Ellipse.h"

// returns true if the conic is solved, we draw things based on this!
bool conic_solved(double *conic_param, double *ellipse_param) {
    
    /**
    * elliptical conic equ: ax^2 + bxy + cy^2 + dx + ey + f = 0
    * (this is done under the conic_params)
    * the great thing about this is that it returns the cartesian
    * coordinates for the solved conic
    */
    
    double a = conic_param[0]; // all the elliptical conic forumla parameters
    double b = conic_param[1]; // shorter variable names were used to reduce file size
    double c = conic_param[2]; // * and cleaner code * !
    double d = conic_param[3];
    double e = conic_param[4];
    double f = conic_param[5];
    
    // note to self, a and b represent major(a) and minor(b) axis
    
    double theta = atan2(b, a - c) / 2; // this retruns the ellipse orientation at a given point
    
    double ct = cos(theta);
    double st = sin(theta);
    
    double ap = a*ct*ct + b*ct*st + c*st*st; // a*ct^2 + b*ct*st + c*st^2
    double cp = a*st*st - b*ct*st + c*ct*ct; // return the scaled major (ap), and minor (cp)
    
    // doubles cx and cy return the translations that are being preformed
    // cx and cy = center of the ellipse, hints the division
    double cx = (2*c*d - b*e) / (b*b - 4*a*c);
    double cy = (2*a*e - b*d) / (b*b - 4*a*c);
    
    // return the scale factor on the ellipse, again using the elliptical conic formula
    double val = a*cx*cx + b*cx*cy + c*cy*cy; // a*cx^2 + b*(cx*cy) + c*cy^2
    double scale_fact = val - f;
    
    // handle imaginary function results and return false if called
    if(scale_fact / ap <= 0 || scale_fact / cp <= 0) {
        cout << "PARAM_IMAG: elliptical parameters are imaginary!" << endl;
        // set blocks of mem on ellipse_param to the size of a double * 5
        // sizeof(double) is 8 bytes (in this case), multiplied by 5 this gives 40 bytes
        // most compilers will intepret this differently (double sizes)
        memset(ellipse_param, 0, sizeof(double) * 5);
        return false; // did not solve because parameters are imaginary
    }
    
    ellipse_param[0] = sqrt(scale_fact / ap);
    ellipse_param[1] = sqrt(scale_fact / cp);
    ellipse_param[2] = cx;
    ellipse_param[3] = cy;
    ellipse_param[4] = theta;
    
    return true; // looks like we solved a conic and can
                // now continue onto drawing and mapping!
    
}

// constuctor
trackPupil::trackPupil () {
    foundABlob = false; // we haven't found blob yet
}

void trackPupil::setup(int width, int height) {
    w = width;
    h = height;
    
    // allocate and set image sizes (creating images)
    imageColor.allocate(w, h);
    imagePreWarpGrayAdv.allocate(w, h);
    imagePreModGrayAdv.allocate(w, h);
    imageGrayAdv.allocate(w, h);
    imageThreshold.allocate(w, h);
    
    // for the newly added masking
    edgeMask.allocate(w, h);
    edgeMaskInverted.allocate(w, h);
    
    threshold = 60;      // probably should stay constant
    // removed variable inits
    
    /**
    * Checking to see if we can apply filters to the image
    * these will be overloaded and changed in the main class
    * based on the user interface xml
    *
    * default filter values are also applied here, can be overloaded
    */
    
    useGamma = false;
    gamma = 1.0f;
    
    useBlur = true;
    blur = 5;
    
    useContrastBrightness = false;
    contrast    = 0.2;
    brightness  = 0.0;
    
    dilateImage = false;
    dilationNumber = 1;
    
    memStore = cvCreateMemStorage(0); // hullz
    
    xFlipped = false;
    yFlipped = false;
    
    // end image filters, can add more above - TODO
    
    // masking initial value
    edgeStatPos.set(w / 2, h / 2);
    edgeOuterRad = 350;
    edgeInnerRad = 250;
    edgePix = new unsigned char[(int)(w*h)];
    
    calcEdgePix(); // calculate the edge pixels on setup
}

// tells if the image should be flipped on x, y
void trackPupil::flip(bool flipX, bool flipY) {
    xFlipped = flipX;
    yFlipped = flipY;
}

// return the pupil point, this method keeps encapsulation
// and maintains the idea of a secure class.
ofPoint trackPupil::returnPupilPoint() {
    return currentPupilPoint;
}

// calculate the pixel edges when the mask is applied
// limits what we can see of a certain image so I can block
// out annoying eyelashes or glasses frames
void trackPupil::calcEdgePix() {
    for(int y = 0; y < w; y++) {
        for(int x = 0; x < h; x++) {
            float dx = y - edgeStatPos.x;
            float dy = x - edgeStatPos.y;
            float dist = sqrt(dx*dx + dy*dy); // simple distance
            float pct = (dist - edgeInnerRad) / (edgeOuterRad - edgeInnerRad);
            if(pct < 0)
                pct = 0;
            if(pct > 1)
                pct = 1;
            
            edgePix[(int)(x*w+y)] = pct * 255; // inverse mat
        }
    }
    
    edgeMask.setFromPixels(edgePix, w, h);
    edgeMaskInverted = edgeMask;
    edgeMaskInverted.invert(); // invert the image
}

void trackPupil::update(ofxCvGrayscaleImage &camGrayImage, float threshold, float minSize, float maxSize, float minSquare) {
    
    // set the content of the image before warp
    imagePreWarpGrayAdv.setFromPixels(camGrayImage.getPixels(), camGrayImage.width, camGrayImage.height);
    
    if(xFlipped || yFlipped) {
        imagePreWarpGrayAdv.mirror(xFlipped, yFlipped);
    }
    
    // flipping and switching out the images
    imageGrayAdv = imagePreWarpGrayAdv;
    imagePreModGrayAdv = imageGrayAdv;
    
    
    
    // check if the image uses gamma or bc
    if(useContrastBrightness == true)
        imageGrayAdv.applyBrightnessContrast(brightness, contrast);
    if(useGamma == true)
        imageGrayAdv.applyMinMaxGamma(gamma);
    if(useBlur == true)
        imageGrayAdv.blur(blur);
    
    // mask the image
    imageGrayAdv += edgeMask;
    
    // equate the threshimage to the grayimage
    imageThreshold = imageGrayAdv;
    
    // apply even contrasts and the threshold value to the image
    imageThreshold.contrastStretch();
    imageThreshold.threshold(threshold, true); // false = dont inverse
    
    // dilate and preform ROI near what I want
    imageThreshold.setROI(currentPupilPoint.x - 50, currentPupilPoint.y - 50, 100, 100);
    if(dilateImage) { // if we want to use dilation
        // loop for the dilation number
        for(int i = 0; i < dilationNumber; i++) {
            imageThreshold.dilate(); // dilate
        }
    }
    imageThreshold.resetROI(); // reset the dilation if else
    
    foundABlob = false;
    int foundWhat = 1;
    
    // find contours in the image based on the image threshold, return the number of them to int
    int numContour = contourFinder.findContours(imageThreshold, minSize, maxSize, 100, false, true);
    // if we find a contour
    if(numContour) {
        for(int j = 0; j < numContour; j++) {
        // set float to the ratio of width/height contours based on the number found
        // this uses a ternary expression (if/else) ": ?"
            float ratioOfContours = contourFinder.blobs[j].boundingRect.width < contourFinder.blobs[j].boundingRect.height ?contourFinder.blobs[j].boundingRect.width / contourFinder.blobs[j].boundingRect.height :
                contourFinder.blobs[j].boundingRect.height / contourFinder.blobs[j].boundingRect.width ;
            
            float arcl = contourFinder.blobs[j].length; // cirlces in lengths
            float area = contourFinder.blobs[j].area; // area of contours
            // compactness of the contours expressed as a ratio
            float compactRatio = (float)((arcl*arcl / area) / FOUR_PI); // 4 * PI
            
            // dont break if the statement evaluates (would throw something otherwise)
            if(useCompactness == true && compactRatio > maxCompactness)
                continue;
            
            // debug features below
            //cout << "Compacts: " << compactRatio << endl;
            
            if(ratioOfContours > minSquare) {
                // try to avoid tracking parrallelograms and triangles
                currentPupilPoint = contourFinder.blobs[j].centroid;
                currentNormalPoint.x = currentPupilPoint.x;
                currentNormalPoint.y = currentPupilPoint.y;
                
                currentNormalPoint.x /= w;
                currentNormalPoint.y /= h;
                
                // choose the contour that matches the formulas and decalre found
                foundABlob = true;
                foundWhat = j; // what contour did we choose?
                break; // escape the loop after declaring blobs
            }
        }
    }
    
    /**
    * A little note on convex hull and what it is:
    * A convex hull is the smallest convex set that contains
    * X in a geometric plane.
    * Convex Set - Segments inside an object in a geometric plane
    */
    
    // -1 = false
    if(foundABlob && foundWhat != -1) {
    // create sequents of elements that can grow. parameters include size and type.
    // for ptseq we are using an undefined sequence (generic)
        CvSeq *ptseq = cvCreateSeq(CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvContour), sizeof(CvPoint), memStore);
        CvSeq *hull;
        CvPoint pt0; // points
        
        // loop through the contour points and set the cvpoint equal
        for(int i = 0; i < contourFinder.blobs[foundWhat].nPts; i++) {
            pt0.x = contourFinder.blobs[foundWhat].pts[i].x;
            pt0.y = contourFinder.blobs[foundWhat].pts[i].y;
            cvSeqPush(ptseq, &pt0); // add the points to the end of the sequence
        }
        
        // find the convex hull for a set of points
        hull = cvConvexHull2(ptseq, 0, CV_CLOCKWISE, 0);
        int numberHulls = hull -> total;
        
        // --- ellipse solving time ---
        int myn = numberHulls;
        
        float x[myn], y[myn];
        float theta;
        
        double params[6];
        double ellipticalParams[5];
        
        // Vector math references for an ellipse
        FitEllipse ellipse;
        
        for(int i = 0; i < myn; i++) {
            CvPoint pt = **CV_GET_SEQ_ELEM(CvPoint*, hull, i);
            x[i] = pt.x;
            y[i] = pt.y;
        }
        
        // 6 parameters
        double xc, yc, xa, ya, la, lb;
        ellipse.apply(x, y, myn);
        
        // solve for the conic params using the ellipse definitions
        params[0] = ellipse.Axx; // solve for all the possible coordinates
        params[1] = ellipse.Axy;
        params[2] = ellipse.Ayy;
        params[3] = ellipse.Ax;
        params[4] = ellipse.Ay;
        params[5] = ellipse.Ao;  // including orientations
        
        bool b0j = conic_solved(params, ellipticalParams);
        ofxCvBlob tempBlob;
        
        // if we can solve the conic...
        if(b0j == true) {
            // redefine the parameters based on the new parameters
            float axis_a = ellipticalParams[0];
            float axis_b = ellipticalParams[1];
            
            float cx = ellipticalParams[2];
            float cy = ellipticalParams[3];
            
            theta = ellipticalParams[4];
            
            float aspect = axis_b / axis_a;
            
            // loop until 5, so 4 loops
            for(int i = 0; i < 5; i ++) {
                // set the new params equal to the solved parmas
                trackedEyeEllipse.conicEllipseParam[i] = ellipticalParams[i];
            }
            
            // check this method, its important!
            int res = 24;
            //vector<ofxPoint2f> pointsToRotateOn (resolution);
            //ofxPoint2f pointsToRotateOn[resolution];
            ofxPoint2f ptsForRot[24];
            
            
            for(int i = 0; i < res; i++) {
                float t = TWO_PI * (float)i / (float)res;
                float ex = cx + (axis_a * cos(t));
                float ey = cy + (axis_b * sin(t));
                ptsForRot[i].set(ex, ey);
            }
            
            for(int i = 0; i < res; i++) {
                ptsForRot[i].rotate(theta * RAD_TO_DEG, ofxPoint2f(cx, cy));
            }
            
            currentPupilPoint.set(cx, cy);
            currentNormalPoint.x = currentPupilPoint.x;
            currentNormalPoint.y = currentPupilPoint.y;
            currentNormalPoint.x /= w;
            currentNormalPoint.y /= h;
        } else {
            foundABlob = false; // we did not find a blob
        }
        
        cvRelease((void **) &hull); // release the hull storage (mem)
    }
}

void trackPupil::draw(float x, float y, float tw, float th) {
    ofPushMatrix();
        ofTranslate(x, y, 0);
        ofScale(tw / (w*2), th / h, 1);
    
        ofSetColor(255, 255, 255); // white
    
        // here is where we draw the images in the window
        imageGrayAdv.draw(0, 0);
        imageThreshold.draw(imageThreshold.width, 0);
        contourFinder.draw(imageThreshold.width, 0);
    
        // if we have a point draw a circle where it is
        if(currentPupilPoint != ofPoint(0, 0)) {
            ofPushStyle();
                ofFill();
                ofSetColor(0, 0, 255);
                ofCircle(x + imageThreshold.width + currentNormalPoint.x * imageThreshold.width, y + currentNormalPoint.y * imageThreshold.height, 4);
            ofPopStyle();
        }
    ofPopMatrix();
    
    ofSetHexColor(0xffffff); // white in hex
    ofDrawBitmapString("Loaded blob drawing component!", x, y + 10); // helps debug
}