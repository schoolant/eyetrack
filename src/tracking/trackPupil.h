//
//  trackPupil.h
//  EyeTrack_Updated
//  2013 - revision 2
//

#ifndef EyeTrack_Updated_trackPupil_h
#define EyeTrack_Updated_trackPupil_h

#include "ofMain.h"
#include "ofxCvGrayscaleAdvanced.h"
#include "ofVectorMath.h"
#include "ofxOpenCv.h"

// trackedEllipse handles the drawing of openGL graphics over the pupil

class trackedEllipse {
    public:
    
    float getWidth(){return 640;} // makes it easier to make changes later
    float getHeight(){return 480;}
    
    float conicEllipseParam[5]; // parameters for soliving the elliptical conics
    
    void draw(float x, float y) {
        draw(x, y, getWidth(), getHeight()); // uses the other draw function below
    }
    
    void draw(float x, float y, float w, float h) {
        // Start off by assigning the parameters to their spot in the array conicEllipseParam
        float axis_a = conicEllipseParam[0]; // x, y
        float axis_b = conicEllipseParam[1];
        
        float cx     = conicEllipseParam[2]; // conic x , conic y
        float cy     = conicEllipseParam[3];
        
        float theta  = conicEllipseParam[4] * RAD_TO_DEG; // RAD_TO_DEG = deg * (180/PI)
        float aspect = axis_b / axis_a;                   // gives us a nice ratio
        
        int res = 24;
        glPushMatrix(); // open up the image matrix and start drawing pixels inside of it. Modify the matrix
        
            glTranslatef(x, y, 0);      // multiply the current mat by the translation mat
                                        //(x,y,z) vectors are translated
            
                                        // multiply the current mat by scaling mat (x,y,z) are scaled
            glScalef((float)w / (float)getWidth(), (float)h / (float)getHeight(), 1);
        
            glTranslatef(cx, cy, 0);    // translate on the conics
            glRotatef(theta, 0, 0, 1);  // multiply current mat by rotate mat
                                        //(angleOfRot, vectorX, vectorY, vectorZ)
        
            // TODO - glColor3ub(r,g,b) for full spectrum (profile it!)
            // outline color
            glColor3ub(236, 118, 0);         // sets the current color (r,g,b) 1 = full intensity
        
            /**
            * As explined by the openGL docs, GL_LINE_LOOP does the following:
            *   "Draws a connected group of line segments from the first vertex to the last, 
            *    then back to the first. Vertices n and n1 define line n. The last line, however, 
            *    is defined by vertices N and 1. N lines are drawn."
            */
            
            glBegin(GL_LINE_LOOP);      // specify primitives that will be created from
                                        // verticies betweem glBegin() and glEnd()
        
                for(int i = 0; i < res; i++) {
                    float t = TWO_PI * (float)i / (float)res;
                    float ex = (axis_a * cos(t));
                    float ey = (axis_b * sin(t));
                    glVertex2f(ex, ey); // specify a vertex, point, line, and poly. Takes x and y
                }
        
            glEnd();
        
            // overlay color
            glColor4ub(255, 205, 34, 100);    // sets current color with alpha channel support (r,g,b,a)
        
            /**
            * As explained by openGL docs, GL_POLYGON does the following:
            *   "Draws a single, convex polygon. Vertices 1 through N define this polygon."
            */
        
            glBegin(GL_POLYGON);
                
                for(int i = 0; i < res; i++) {
                    float t = TWO_PI * (float)i / (float)res;
                    float ex = (axis_a * cos(t));
                    float ey = (axis_b * sin(t));
                    glVertex2f(ex, ey); // specify a vertex, point, line, and poly. Takes x and y
                }
        
            glEnd();
        
        for(int i = 0; i < res; i += 4) {
            float t = TWO_PI * (float)i / (float)res;
            float ex = (axis_a * cos(t));
            float ey = (axis_b * sin(t));
            ofLine(0, 0, ex, ey);
        }
        
        glPopMatrix(); // close the matrix edit, we're done modifiying it
    }
};

class trackPupil {
    public:
    
    trackPupil();   // constructor
    
    void setup(int width, int height);
    
    // what we check when the tracker updates
    void update(ofxCvGrayscaleImage &camGrayImage, float threshold,
    float minSize, float maxSize, float minSquare = 0.7);
    
    void draw(float x, float y, float tw, float th); // what gets drawn
    void flip(bool flipX, bool flipY);
    
    ofPoint returnPupilPoint();     // function that returns the current point of the pupil
    
    // best thing ever - edge masking.
    ofxCvGrayscaleAdvanced  imageGrayAdv;           // regular grayscale image (advanced)
    ofxCvGrayscaleAdvanced  imagePreModGrayAdv;     // gray image before modification
    ofxCvGrayscaleAdvanced  imagePreWarpGrayAdv;    // image before warps and effects
    ofxCvGrayscaleAdvanced  imageThreshold;         // image responsible for the threshold
    
    ofxCvColorImage         imageColor;             // regular color image, displayed as result
    ofxCvContourFinder      contourFinder;          // responsible for finding image contours (native CV)
    
    void calcEdgePix();
    ofPoint edgeStatPos;
    
    float edgeInnerRad, edgeOuterRad;
    unsigned char *edgePix;
    
    ofxCvGrayscaleImage edgeMask;
    ofxCvGrayscaleImage edgeMaskInverted;
    
    bool foundABlob;        // have we found a blob in the current frame?
    
    CvMemStorage *memStore; // memory storage, for buffers
    
    trackedEllipse trackedEyeEllipse; // the actual ellipse
    // defined above
    
    ofPoint currentPupilPoint; // curent locations of the pupil
    ofPoint currentNormalPoint;  // and normal points in the image
    
    // Filter settings and other image effect options need to be integrated into this file
    // the current effects and settings that have been written are declared below
    
    bool useCompactness;  // image compactness, for contours only
    float maxCompactness; // try to find a better contour based on this
    
    bool    useGamma; // image gamma, use it or loose it?
    float   gamma;
    
    bool    useBlur;  // image blur, for a smoother calculation
    float   blur;
    
    bool    useContrastBrightness;
    float   contrast; // image contrast and brightness settings. Seperate them?
    float   brightness;
    
    bool    dilateImage;        // does the image need to be dilated?
    int     dilationNumber;
    
    bool    xFlipped, yFlipped; // flip on x and y axis - does not affect tracking
                                // for your viewing pleasures - affecting masks though!
    
    float   w, h;               // threshold settings
    float   threshold;
    
};

#endif
