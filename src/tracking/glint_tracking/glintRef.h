//
//  glintRef.h
//  EyeTrack_Updated
//
// Mark's and applies glints in an image
// does not completely solve for a glint, just
// tells where they are located in the image
// and contains properties about their location

#ifndef EyeTrack_Updated_glintRef_h
#define EyeTrack_Updated_glintRef_h

#include "ofMain.h"
#include "ofxOpenCv.h"

// define left and right glints, because there are only going to be
// a maximum of two glints in the pupil image
enum glintEleNum { GLINT_ELE_LEFT = 0, GLINT_ELE_RIGHT = 1 };

// glint bottom and top sectors of the image
enum glintCombineNum { GLINT_ELE_BOTTOM = 0, GLINT_ELE_TOP = 1 };

// area and the rectangle that encompasses the two glints
typedef struct {
    int area;
    ofRectangle boundRect; } glintUnit;

typedef struct {
    // for the 2 glints in the image
    vector <glintUnit> glints[2];
    float areaAvg[2];
    float widthAvg[2];
    float heightAvg[2];
    float distAvg; } glintHist;

class glintRef {
    public:
    
    void setup();
    void clear();
    void calcAvg();
    
    // add glints to the image
    void addGlints(ofxCvBlob &leftRefBlob, ofxCvBlob &rightRefBlob, bool bool_topGlints);
    bool chkSize(ofxCvBlob &blob, int glintNum, float minPct, float maxPct);
    
    // return the average distance of the glints in the enclosing box
    float   getAvgDist(bool bool_topGlints);
    int     getNumFrames(bool bool_topGlints);
    
    bool bool_needCalc;
    glintHist glintH[2];
};

#endif
