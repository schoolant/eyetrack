//
//  glintDetector.h
//  EyeTrack_Updated
//

#ifndef EyeTrack_Updated_glintDetector_h
#define EyeTrack_Updated_glintDetector_h

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxCvGrayscaleAdvanced.h"
#include "ofxVectorMath.h"

#include "glintRef.h"
#include "glintLine.h"

class glintDetector {
    public:
    
    void setup(int eye_width, int eye_height, float _ratioMag, float im_width, float im_height);
    bool update(ofxCvGrayscaleAdvanced &imageBlackEye, float threshold, float minSize, float maxSize, bool bool_brightEyeChk);
    void draw(float x, float y, bool bool_error);
};


#endif
