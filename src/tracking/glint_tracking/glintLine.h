//
//  glintLine.h
//  EyeTrack_Updated
//


#ifndef EyeTrack_Updated_glintLine_h
#define EyeTrack_Updated_glintLine_h

#include "ofMain.h"
#include "ofxOpenCv.h"

enum glintNum {
  GLINT_ELE_BOTTOM_LEFT = 0,
  GLINT_ELE_BOTTOM_RIGHT = 1,
  GLINT_ELE_TOP_LEFT = 2,
  GLINT_ELE_TOP_RIGHT = 3,
  GLINT_IN_BRIGHT_EYE = 4
};

// typedef for a line segment
typedef struct {
    int xStart, yStart;
    int xEnd, yEnd;
    int dist;
}   lineSeg;

class glintLine {
    public:
  
    void setup(int imagePupil_width, int imagePupil_height);
    void update(ofxCvGrayscaleImage &pupilImage, int glintNum, ofxCvContourFinder &contourFinder
                , bool bool_glintInBrightPupil, ofxCvContourFinder &brightContourFinder);
    void draw(int x, int y);
  
    // check if the glint is in the bright sector
    void checkGlintBright();
  
    // vector of linesegments
    vector<lineSeg> lineSegs;
    ofxCvGrayscaleImage   imageStripes;
    ofxCvContourFinder    lineFinder;
  
    // define values of lines and
    // the glint "id" numbers
    int width, height;
    float minDist, maxDist;
    int glintLeftNum, glintRightNum;
    bool bool_removeLine;
  
    // check if a line crosses a bright pupil glint
    bool lineCrossBrightGlint(const lineSeg &seg, ofxCvContourFinder &brightContourFinder);
    // check if a line is in range of a glint
    bool lineRange(const lineSeg &seg);
    
};

#endif
