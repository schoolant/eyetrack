//
//  glintLine.cpp
//  EyeTrack_Updated
//


#include "glintLine.h"

// is x line in range of the minDist and maxDist?
bool glintLine::lineRange(const lineSeg &seg) {
    return seg.dist < minDist || seg.dist > maxDist;
}

// does x line cross a bright pupil glint?
bool glintLine::lineCrossBrightGlint(const lineSeg &seg, ofxCvContourFinder &brightContourFinder) {
    int out = 0;
    
    // check if the lines our out of the bright glints
    // or if they are inside the bright glints
    for(int i = 0; i < brightContourFinder.blobs.size(); i++) {
        if(seg.xStart > brightContourFinder.blobs[i].centroid.x
           || seg.xEnd < brightContourFinder.blobs[i].centroid.x) {
            out++; // number of outside 
        }
    }
    
    // if there are less lines out of the blobs
    // found from the contours then
    // return false, else return true
    if(out < brightContourFinder.blobs.size())
        return false;
    else
        return true;
}

// what happens on init?
void glintLine::setup(int imagePupil_width, int imagePupil_height) {
    width = imagePupil_width;
    height = imagePupil_height;
    
    // distance of the glints in pixels
    minDist = 50;
    maxDist = 200;
    
    // allocate pixels to the image based on the pupilimage w/h
    imageStripes.allocate(width, height);
    bool_removeLine = false; // no line being removed
}

// what happens on class update?
void glintLine::update(ofxCvGrayscaleImage &pupilImage, int glintNum, ofxCvContourFinder &contourFinder, bool bool_glintInBrightPupil, ofxCvContourFinder &brightContourFinder) {

    // if there are currently two glints in the image
    if(glintNum == 2) {
        // remove the previously found segments
        lineSegs.clear();
        unsigned char *pixels = pupilImage.getPixels();
        
        // loop through the image width and height
        for(int y = 0; y < pupilImage.height; y++) {
            lineSeg tempLineSeg;
            bool bool_started = false;
            for(int x = 0; x < pupilImage.width; x++) {
                int pixa = pixels[y*pupilImage.width+x];
                int pixb = pixels[y*pupilImage.width+x+1];
                
                // if the pixel positions are both white...
                
                /**
                * The reason I'm using white pixels as a factor
                * is because all the glints in the image will
                * be 100% 255 due to the thresholding. Light will
                * always appear a bright white in the images.
                */
                
                if((pixa == 255) && (pixb == 255)) {
                    if(bool_started) {
                        tempLineSeg.xEnd = x;
                        tempLineSeg.yEnd = y;
                        tempLineSeg.dist = tempLineSeg.xEnd - tempLineSeg.yStart;
                        
                        // add a line segment to the vector of line segments
                        lineSegs.push_back(tempLineSeg);
                        bool_started = false; // we're done adding segs
                    }
                }
            }
        }
        
        // if a line needs to be removed during update
        if(bool_removeLine) {
            // loop through the segment vector and determine what needs
            // to be marked for removal
            for(int i = 0; i < lineSegs.size(); i++) {
                if(lineRange(lineSegs[i])) {
                    // erase starting at the beggining
                    lineSegs.erase(lineSegs.begin() + i);
                    i--;
                }
            }
            
            // erase the glints in the bright eye spectrums
            for(int i = 0; i < lineSegs.size(); i++) {
                if(bool_glintInBrightPupil) {
                    if(lineCrossBrightGlint(lineSegs[i], brightContourFinder)) {
                        lineSegs.erase(lineSegs.begin() + i);
                        i--;
                    }
                }
            }
        }
        
        cvSetZero(imageStripes.getCvImage());
        unsigned char *stipePix = imageStripes.getPixels();
        
        // develop more line fitting tomorrow
		for(int i = 0; i < lineSegs.size(); i++) {
			// define temp copies
			int xStart = lineSegs[i].xStart;
			int xEnd = lineSegs[i].xEnd;
			int y = lineSegs[i].yStart;

			for(int j = xStart; j < xEnd; j++) {
				// set the striped pixel color to white
				stipePix[y*imageStripes.width+j] = 255;
			}
		}
		// find the glints in the striped pixel image
		// returns the contours and sets blobNum to the number
		// of contours that have been found
		imageStripes.flagImageChanged();
		int blobNum = lineFinder.findContours(imageStripes, 100, 10000, 1, false, true);

		// set the glint "id" numbers
        // we haven't found anything yet
		glintLeftNum = -1;
		glintRightNum = -1;

		// if we've found contours based on
		// the tests that were run through the image...
		if(blobNum > 0) {
			ofRectangle markedLineRect = lineFinder.blobs[0].boundingRect;
            
            // determine the right and left blob sizes
			for(int i = 0; i < contourFinder.blobs.size(); i++) {
                // blob rectangle object
                ofRectangle blobRect = contourFinder.blobs[i].boundingRect;
                if(ofInRange(markedLineRect.x, blobRect.x, blobRect.x + blobRect.width) &&
                   (ofInRange(markedLineRect.y, blobRect.y, blobRect.y + blobRect.height) ||
                    ofInRange(markedLineRect.y + markedLineRect.height, blobRect.y, blobRect.y + blobRect.height))) {
                       glintLeftNum = i;
                   } else if(ofInRange(markedLineRect.x + markedLineRect.width, blobRect.x, blobRect.x + blobRect.width) &&
                        (ofInRange(markedLineRect.y, blobRect.y, blobRect.y + blobRect.height) ||
                         ofInRange(markedLineRect.y + markedLineRect.height, blobRect.y, blobRect.y + blobRect.height))) {
                            glintRightNum = i;
                        }
            }
		}
	}
    
}

// draw the glint lines and contours
void glintLine::draw(int x, int y) {
    ofEnableAlphaBlending();
    ofSetColor(255, 0, 0, 80);
    // drawing both contour image and line image
    imageStripes.draw(x, y);
    lineFinder.draw(x, y);
    ofDisableAlphaBlending();
}