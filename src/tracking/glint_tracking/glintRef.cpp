//
//  glintRef.cpp
//  EyeTrack_Updated
//
// Handles the addition of the glint locations
// in the current pupil image

#include "glintRef.h"

void glintRef::setup() {
    bool_needCalc = false;
}

void glintRef::addGlints(ofxCvBlob &leftRefBlob, ofxCvBlob &rightRefBlob, bool bool_topGlints) {
    glintUnit leftTemp;
    glintUnit rightTemp;
    
    leftTemp.area = leftRefBlob.area;
    rightTemp.area = rightRefBlob.area;
    
    if(bool_topGlints) {
    
        // assign the blobs to contain glints in their areas
        glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].push_back(leftTemp);
        glintH[GLINT_ELE_TOP].glints[GLINT_ELE_RIGHT].push_back(rightTemp);
        // erase false positives based on size, glints are small...
        if(glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].size() > 500) {
            // left
            glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].erase(glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].begin());
            // right
            glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].erase(glintH[GLINT_ELE_TOP].glints[GLINT_ELE_RIGHT].begin());
        }
    } else {
        // perform the same detections for glints on the lower image
        glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].push_back(leftTemp);
        glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_RIGHT].push_back(rightTemp);
        if(glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].size() > 500) {
            // left
            glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].erase(glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].begin());
            // right
            glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].erase(glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_RIGHT].begin());
        }
    }
    
    // after locating the area and position of the
    // glints they now need to be calculated
    bool_needCalc = true;
}


void glintRef::calcAvg() {
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 2; j++) {
            float temp_areaAvg = 0;
            float temp_widthAvg = 0;
            float temp_heightAvg = 0;
            
            // fill the temporary variables with the glint properties
            for(int k = 0; k < glintH[i].glints[j].size(); k++) {
                // temporary values created to assist in the averages
                temp_areaAvg += glintH[i].glints[j][k].area;
                temp_widthAvg += glintH[i].glints[j][k].boundRect.width;
                temp_heightAvg += glintH[i].glints[j][k].boundRect.height;
            }
            
            // assign the average area of glints
            glintH[i].areaAvg[j] = temp_areaAvg / glintH[i].glints[j].size();
            
            // assign the average width and height of glints
            glintH[i].widthAvg[j] = temp_widthAvg / glintH[i].glints[j].size();
            glintH[i].heightAvg[j] = temp_heightAvg / glintH[i].glints[j].size();
        }
        
        // temporary distance average divisor
        float temp_distAvg = 0;
        
        // distance average on the left and right glints in the image
        for(int k = 0; k < glintH[i].glints[GLINT_ELE_LEFT].size(); k++) {
            // huge float that measures the distance of the two points
            // the variable is the average distance, since most likely glints
            // will move a lot.
            temp_distAvg += glintH[i].glints[GLINT_ELE_RIGHT][k].boundRect.x
                            - (glintH[i].glints[GLINT_ELE_LEFT][k].boundRect.x
                            + glintH[i].glints[GLINT_ELE_LEFT][k].boundRect.width);
        }
        
        // assign the average variable based on the temporary distance updated above
        glintH[i].distAvg = temp_distAvg / glintH[i].glints[GLINT_ELE_LEFT].size();
    }
    
    // calculation complete, so we dont need any more calculations
    bool_needCalc = false;
}

bool glintRef::chkSize(ofxCvBlob &blob, int glintNum, float minPct, float maxPct) {
    int verticalPos, horizontalPos;
    
    // where our glint is in the image
    if(glintNum < 2) {
        // bottom [lower] of the image
        verticalPos = GLINT_ELE_BOTTOM;
        horizontalPos = glintNum;
    } else {
        // top [upper] of the image
        verticalPos = GLINT_ELE_TOP;
        horizontalPos = glintNum - 2;
    }
    
    // check the bounds of the glints in the image and get the average widths and heights
    // to verify if the size of the glints is valid at all. if so return true
    // else return false to signify that the values are not valid.
    if(ofInRange(blob.boundingRect.width, glintH[verticalPos].widthAvg[horizontalPos] * minPct, glintH[verticalPos].widthAvg[horizontalPos] * maxPct)
       && ofInRange(blob.boundingRect.height, glintH[verticalPos].heightAvg[horizontalPos] * minPct, glintH[verticalPos].widthAvg[horizontalPos] * maxPct)) {
        return true;
    } else {
        return false;
    }
}

float glintRef::getAvgDist(bool bool_topGlints) {
    // return the bottom glint avgDist or top
    // based on the boolean that is passed
    if(bool_topGlints)
        return glintH[GLINT_ELE_TOP].distAvg;
    else
        return glintH[GLINT_ELE_BOTTOM].distAvg;
}

int glintRef::getNumFrames(bool bool_topGlints) {
    // return the frame size based on passed boolean
    if(bool_topGlints)
        return glintH[GLINT_ELE_TOP].glints[GLINT_ELE_LEFT].size();
    else
        return glintH[GLINT_ELE_BOTTOM].glints[GLINT_ELE_LEFT].size();
}

// clear out the array and reset everything
void glintRef::clear() {
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 2; j++) {
            // reset averages for next calculation
            glintH[i].glints[j].clear();
            glintH[i].widthAvg[j] = 0;
            glintH[i].heightAvg[j] = 0;
            glintH[i].areaAvg[j] = 0;
        }
        glintH[i].distAvg = 0;
    }
}