//
//  trackingHandler.h
//  EyeTrack_Updated
//
// handles all of the tracking stuff!

#ifndef EyeTrack_Updated_trackingHandler_h
#define EyeTrack_Updated_trackingHandler_h

#include "ofMain.h"
#include "trackPupil.h"
#include "videoLoader.h"
#include "ofxControlPanel.h"

class trackingHandler {
public:
    
    trackingHandler();
    
    void setup();
    void update();
    void draw();
    
    void trackBlobs();
    void setupVideo();
    void videoSettings();
    
    // for the gui
    void loadGui();
    void updateGui();
    
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased();
    
    // gui panel
    ofxControlPanel gui;
    
    ofPoint givePupilPoint();
    bool    foundEyeInNewFrame();
    
    bool foundPupil;
    int minSizeBlob, maxSizeBlob;
    float threshold;
    ofPoint pupilPoint;
    
    // components
    videoLoader vidLoader;
    trackPupil  pupilTrack;
};

#endif
