/**
* This file needs to (does now) load GUI settings
* and change variables based on toggles and switches.
*
* Future idea - all gui settings in xml and variables
* will be switched inside of this file (currently implemented in rev_2_B_6)
*/

#include "trackingHandler.h"

trackingHandler::trackingHandler() {
    
}

void trackingHandler::setup() {
    vidLoader.setup(); // setup the camera
    loadGui();         // init the GUI
    
    /**
    * Load up all the tracking along with all
    * the dependencies that the tracking needs
    */
    
    // setup() inside of trackPupil.cpp
    pupilTrack.setup(vidLoader.frame_width, vidLoader.frame_height);
    minSizeBlob = 100; // minimum and max sized blobs
    maxSizeBlob = 10000;
    threshold   = 20; // 20 - 23
    foundPupil  = false;
    
    pupilPoint.set(0, 0, 0); // want to add in 3D vector translations...
    
    //videoSettings(); // load video settings on startup - DONE change to gui button
}

// what happens when we load the gui
void trackingHandler::loadGui() {
    // define settings panels
    gui.setup("pupil tracking", 700, 20, 300, 450); // starting panel
    gui.addPanel("tracking_setup", 1, false); // tracking
    gui.addPanel("edge_mask", 1, false); // masks
    gui.addPanel("blob_detect", 1, false); // detection
    
    if (vidLoader.mode == IN_VID) {
        gui.addPanel("vid file settings", 1, false);
    } else {
        gui.addPanel("live vid settings", 1, false);
    }
    
    // twilight gui theme - needs work.
    gui.guiBaseObject::setBackgroundColor(41, 49, 52, 255);
    gui.guiBaseObject::setOutlineColor(103, 140, 177, 255);
    gui.guiBaseObject::setOutlineSelectColor(98, 115, 150, 255);
    
    gui.setBackgroundColor(simpleColor(41, 49, 52, 255));
    gui.setForegroundColor(simpleColor(98, 115, 150, 255));
    gui.setOutlineColor(simpleColor(103, 140, 177, 255), simpleColor(103, 140, 177, 255));
    gui.setTextColor(simpleColor(147, 199, 99, 255));
    
    // TRACKING SETTINGS
    // working with this panel
    gui.setWhichPanel("tracking_setup");
    gui.setWhichColumn(0); // column 0
    // name, xml setting, default val
    gui.addToggle(" Use Contrast / Brightness", "USE_CONTRAST", false);
    gui.addSlider("Contrast", "CONTRAST", 0.2f, 0.0, 1.0f, false);
    gui.addSlider("Brightness", "BRIGHTNESS", -0.02f, -1.0, 3.0f, false);
    // threshold
    gui.addSlider("Threshold", "THRESHOLD", 20, 0, 255, true);
    // gamma
    gui.addToggle(" Use Gamma", "USE_GAMMA", false);
    gui.addSlider("Gamma", "GAMMA", 0.5f, 0.01, 3.0f, false);
    // blur
    gui.addToggle(" Use Image Blur", "USE_IMGBLUR", true);
    gui.addSlider("Blur", "BLUR", 5, 0, 20, false);
    // flip image
    gui.addToggle(" Flip Image Y", "FLIP_X", false);
    gui.addToggle(" Flip Image X", "FLIP_Y", false);
    
    // EDGE MASK SETTINGS
    gui.setWhichPanel("edge_mask");
    gui.setWhichColumn(0);
    gui.addSlider("Inner Mask", "INNER_MASK_VAL", 250, 0, 500, true); // image sizes may vary
    gui.addSlider("Outer Mask", "OUTER_MASK_VAL", 350, 0, 600, true);
    gui.addSlider("Mask Position X", "MASK_POS_X", 320, 0, 640, true);
    gui.addSlider("Mask Position Y", "MASK_POS_Y", 240, 0, 640, true);
    
    // VIDEO SETTINGS
    if(vidLoader.mode == IN_VID) {
        gui.setWhichPanel("vid file settings");
    } else {
        gui.setWhichPanel("live vid settings");
        gui.addToggle(" Load OSX Video Settings", "LOAD_VID_MANAGER", false);
    }
    
    // BLOB DETECTION SETTINGS
    gui.setWhichPanel("blob_detect");
    gui.setWhichColumn(0);
    gui.addToggle(" Dilate Blob Size", "USE_DILATION", false);
    gui.addSlider("Dilations", "DILATIONS", 1, 0, 15, true);
    // min and max blob sizes
    gui.addSlider("Mininum Blob Size", "MIN_BLOB_SIZE", 1000, 0, 5000, true);
    gui.addSlider("Maximum Blob Size", "MAX_BLOB_SIZE", 10000, 0, 50000, true);
    
    // file save and load paths
    gui.addToggle(" Write Settings", "WRITE_SETTINGS", false);
    gui.addToggle(" Load Settings", "LOAD_SETTINGS", false);
    
    // load saved settings
    gui.loadSettings("settings/dataSettings.xml");
}

// what happens when we update the gui
void trackingHandler::updateGui() {
    
    // contrast and brightness
    pupilTrack.useContrastBrightness    = gui.getValueB("USE_CONTRAST");
    pupilTrack.contrast                 = gui.getValueF("CONTRAST");
    pupilTrack.brightness               = gui.getValueF("BRIGHTNESS");
    
    // threshold
    threshold                           = gui.getValueI("THRESHOLD");
    
    // gamma
    pupilTrack.useGamma                 = gui.getValueB("USE_GAMMA");
    pupilTrack.gamma                    = gui.getValueF("GAMMA");
    
    // blur
    pupilTrack.useBlur                  = gui.getValueB("USE_IMGBLUR");
    pupilTrack.blur                     = gui.getValueF("BLUR");
    
    // flipping image
    pupilTrack.flip(gui.getValueB("FLIP_X"), gui.getValueB("FLIP_Y"));
    
    // EDGE MASKING UPDATES - need to recalculate the mask in here
    // get the old values for relcalculating moves of the mask
    int oldMaskX, oldMaskY, oldMaskInner, oldMaskOuter;
    oldMaskX     = pupilTrack.edgeStatPos.x;
    oldMaskY     = pupilTrack.edgeStatPos.y;
    oldMaskOuter = pupilTrack.edgeOuterRad;
    oldMaskInner = pupilTrack.edgeInnerRad;
    
    // set the new mask position
    pupilTrack.edgeInnerRad     = gui.getValueI("INNER_MASK_VAL");
    pupilTrack.edgeOuterRad     = gui.getValueI("OUTER_MASK_VAL");
    pupilTrack.edgeStatPos.x    = gui.getValueI("MASK_POS_X");
    pupilTrack.edgeStatPos.y    = gui.getValueI("MASK_POS_Y");
    
    // recalculate the mask position the same way we calculated it
    if(oldMaskX != pupilTrack.edgeStatPos.x ||
       oldMaskY != pupilTrack.edgeStatPos.y ||
       oldMaskInner != pupilTrack.edgeInnerRad ||
       oldMaskOuter != pupilTrack.edgeOuterRad)
       pupilTrack.calcEdgePix(); // if conditions are met, relcalculate
    
    // BLOB DETECTION UPDATES
    pupilTrack.dilateImage    = gui.getValueB("USE_DILATION");
    pupilTrack.dilationNumber = gui.getValueI("DILATIONS");
    minSizeBlob               = gui.getValueI("MIN_BLOB_SIZE");
    maxSizeBlob               = gui.getValueI("MAX_BLOB_SIZE");
    
    // VIDEO SETTING UPDATES
    if(vidLoader.mode != IN_VID) {
        gui.setWhichPanel("live vid settings");
        if(gui.getValueB("LOAD_VID_MANAGER") == true) {
            #ifdef TARGET_OSX
            // to prevent the fullscreen bug
            ofSetFullscreen(false);
            #endif
            
            vidLoader.grabber.videoSettings();
            gui.setValueB("LOAD_VID_MANAGER", false);
        }
    }
    
    
    // write and load video settings
    if(gui.getValueB("WRITE_SETTINGS")) {
        gui.saveSettings("../../src/settings/guiSettings.xml");
        gui.setValueB("WRITE_SETTINGS", false);
    }
    if(gui.getValueB("LOAD_SETTINGS")) {
        gui.loadSettings("../../src/settings/guiSettings.xml");
        gui.setValueB("LOAD_SETTINGS", false);
    }
}

void trackingHandler::update() {
    // update the video loader
    vidLoader.update();
    
    // if we can graba a new frame go ahead and track blobs
    if(vidLoader.newFrameGrabbed) {
        trackBlobs();
    }
    
    // for the gui
    gui.update();
    updateGui();
}

bool trackingHandler::foundEyeInNewFrame() {
    return foundPupil;
}

ofPoint trackingHandler::givePupilPoint() {
    return pupilPoint;
}

// opens the video settings when called
void trackingHandler::videoSettings() {
    if(true) {
        vidLoader.grabber.videoSettings();
    }
}

void trackingHandler::trackBlobs() {
    pupilTrack.update(vidLoader.grayImage, threshold, minSizeBlob, maxSizeBlob, 0.5f);
    foundPupil = pupilTrack.foundABlob;
    pupilPoint = pupilTrack.returnPupilPoint();
}

void trackingHandler::draw() {
    ofSetColor(255, 255, 255);
    
    //**** DATA IMAGES DRAWING BELOW //
    
    // going to draw images at 1/2 resolutions
    pupilTrack.imagePreModGrayAdv.draw(10, 10, 320, 240);
    pupilTrack.imageGrayAdv.draw(320 + 20, 10, 320, 240);
    pupilTrack.imageThreshold.draw(10, 20 + 240, 320, 240);
    pupilTrack.contourFinder.draw(10, 20 + 240, 320, 240);
    
    ofEnableAlphaBlending(); // enable alpha channel when drawing the blob
    
    // draw the detection
    ofSetColor(255, 255, 255);
    ofFill();
    ofRect(320 + 20, 240 + 20, 320, 240);
    pupilTrack.imagePreModGrayAdv.draw(320 + 20, 240 + 20, 320, 240);
    pupilTrack.trackedEyeEllipse.draw(320 + 20, 240 + 20, 320, 240);
    
    gui.draw(); // draw the gui
}

void trackingHandler::mouseDragged(int x, int y, int button) {
    gui.mouseDragged(x, y, button);
}

void trackingHandler::mousePressed(int x, int y, int button) {
    gui.mousePressed(x, y, button);
}

void trackingHandler::mouseReleased() {
    gui.mouseReleased();
}